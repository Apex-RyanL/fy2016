# FY 2016 GOALS #





### Desired Structure ###

    -+ build/

       -+ dev/
            -+ CSS_Book

       -+ prod/
            -+ CSS_Book


## Gulp Tasks ##

All tasks associated with this project.

### Start ###

Builds the project and watches the files for changes. It should rebuild the project when a change is made.
```shell
    $ gulp start
```

### Clean ###

Remove all files and directories from build/
```shell
    $ gulp clean
```
Remove all files and directores from build/dev
```shell
    $ gulp clean.dev
```
Remove all files and directories from build/prod
```shell
    $ gulp clean.prod
```

### Build ###

Builds all files for the project
```shell
    $ gulp build
```
Builds all files in dev
```shell
    $ gulp build.dev
```
Builds all files in prod
```shell
    $ gulp build.prod
```



## Update Resources ##

Update TSD files by running
```shell
    $ sudo tsd update -so
```