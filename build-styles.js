
var fs = require('fs');

module.exports = (function () {

    // Build style.css for each chapter config (place in build/dev/public/config/chapter(n)/style.css)
    function buildStyles(callback) {

        var pathToBuild = './build/dev/public/configs/';
        var lineline = '\n\n';

        var chapter;
        var chapterName, filePath;
        var CSS_Markup;
        var i, n = 10;

        for (i = 0; i < n; ++i) {
            chapterName = 'chapter' + (i+1).toString();
            filePath = pathToBuild + chapterName + '/styles.css';
            chapter = require(pathToBuild + chapterName + '/config.js');
            CSS_Markup = lineline;

            // Set the chapter value to the inner chapter object
            chapter = chapter[chapterName];

            if (chapter.commonCSS) {
                CSS_Markup += chapter.commonCSS + lineline;
            }

            chapter.sections.forEach(function (section) {
                section.concepts.forEach(function (concept) {
                    CSS_Markup += concept.styles + lineline;
                });
                CSS_Markup += lineline;
            });

            fs.closeSync(fs.openSync(filePath, 'w'));
            fs.writeFileSync(filePath, CSS_Markup);
        }

        callback();
    }

    return buildStyles;

});