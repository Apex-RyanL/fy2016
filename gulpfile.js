
// NPM Modules
const fs            = require('fs');
const del           = require('del');
const join          = require('path').join;
const gulp          = require('gulp');
const gulpTsc       = require('gulp-typescript');
const tsd           = require('gulp-tsd');
const gulpif        = require('gulp-if');
const plumber       = require('gulp-plumber');
const typescript    = require('typescript');
const runSequence   = require('run-sequence');
const exec          = require('child_process').exec;
const tslint        = require('gulp-tslint');
const buildStyles   = require('./build-styles');

// Constant Declarations
const PUBLIC_DIR    = 'public';
const RUNNER_DIR    = 'runner';
const BUILD_DIR     = 'build';
const TASKS_DIR     = 'tasks';
const LIBS_DEV      = [
    './node_modules/es6-shim/es6-shim.min.js',
    './node_modules/es6-shim/es6-shim.map',
    './node_modules/systemjs/dist/system.js',
    './node_modules/systemjs/dist/system-polyfills.js',
    './node_modules/angular2/es6/dev/src/testing/shims_for_IE.js',
    './node_modules/angular2/bundles/angular2.js',
    './node_modules/angular2/bundles/angular2-polyfills.js',
    './node_modules/rxjs/bundles/Rx.js',
    './node_modules/rxjs/bundles/Rx.min.js.map'
];
const TS_DIR = [
    PUBLIC_DIR,
    RUNNER_DIR
];

/**
 *  =================================
 *           DEFAULT TASK
 *  =================================
 */

gulp.task('default', (done) => {
    runSequence(
        'build',
        done
    )
});

/**
 *  =================================
 *            CLEAN TASK
 *  =================================
 */

gulp.task('clean', (done) => { cleanDir(join(BUILD_DIR), done); });

/**
 *  =================================
 *
 *            BUILD TASKS
 *
 *  - Transpile .ts -> .js
 *  - Copy files to build directory
 *  =================================
 */

gulp.task('build', (done) => {
    runSequence('clean', 'build.dev', done);
});

gulp.task('build.dev', (done) => {
    runSequence(
        'transpile.runner.ts',
        'transpile.public.ts',
        ['copy.libs.dev', 'copy.public.dev', 'copy.runner.dev'],
        'build.config',
        'tslint.public',
        done
    );
});

gulp.task('build.config', (done) => {
    var buildConfig = buildStyles();
    buildConfig(done);
});

gulp.task('tslint.public', () => {
    return gulp.src( 'public/**/*.ts' )
        .pipe(tslint())
        .pipe(tslint.report('full', {
            emitError: false
        }));
});

gulp.task('transpile.public.ts', () => {
    return buildTS(PUBLIC_DIR);
});
gulp.task('transpile.runner.ts', () => {
    return buildTS(RUNNER_DIR);
});

gulp.task('copy.libs.dev', function () {
    return gulp.src(LIBS_DEV.concat(['public/libs/*']))
        .pipe(gulp.dest('build/dev/public/libs'));
});
gulp.task('copy.public.dev', () => {
    return gulp.src([
            '!public/**/*.ts',
            'public/**/*'
        ])
        .pipe(gulp.dest('build/dev/public'));
});
gulp.task('copy.runner.dev', () => {
    return gulp.src([
            '!runner/**/*.ts',
            'runner/**/*'
        ])
        .pipe(gulp.dest('build/dev/runner'));
});

gulp.task('install.typings', (done) => {
    tsd({
        command: 'reinstall',
        config: './tsd.json'
    }, done);
});
gulp.task('post.install.typings', (done) => {
    var cmd = 'sudo chmod -R 777 ./typings/';
    execChildProcess(cmd, done);
});

gulp.task('watch.public', () => {
    gulp.watch([
        join(PUBLIC_DIR, '**/*')
    ], ['build']);
});
gulp.task('watch.runner', () => {
    gulp.watch([
        join(RUNNER_DIR, '**/*')
    ], ['build']);
});
gulp.task('watch.tasks', () => {
    gulp.watch([
        join(TASKS_DIR, '**/*')
    ], ['build']);
});

/**
 *  =================================
 *            MAIN TASKS
 *  =================================
 */

gulp.task('start', () => {
    runSequence(
        'build',
        ['watch.public', 'watch.runner', 'watch.tasks']
    );
});

gulp.task('postinstall', function (done) {
    console.log('Post Install:');
    runSequence('install.typings', 'post.install.typings', done);
});

/**
 *  =================================
 *           HELPER METHODS
 *  =================================
 */

function cleanDir(dir, done) {
    del([dir]).then(() => { done(); });
}

function execChildProcess(cmd, cb) {
    if (Object.prototype.toString.call(cmd) === '[object Array]') {
        cmd = cmd.join(' && ');
    }

    var childProcess = exec(cmd);

    childProcess.stdout.on('data', function (data) {
        console.log(data.toString());
    });

    childProcess.stderr.on('data', function (data) {
        console.log(data.toString());
    });

    childProcess.on('exit', function () {
        cb && cb();
    });
}

var buildTS = (function () {

    var tsDirectories = {};
    TS_DIR.forEach(function(dir) {
        tsDirectories[dir] = gulpTsc.createProject('tsconfig.json', { typescript: typescript });
    });

    return function _buildTypescriptDirectory (buildDir) {

        var transpile = gulp.src([join(buildDir, '**/*.ts')])
            .pipe(plumber())
            .pipe(gulpTsc(tsDirectories[buildDir]));

        return transpile.js
            .pipe(gulp.dest(join('build/dev', buildDir)));
    };

})();