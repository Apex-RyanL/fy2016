
import { Component, Type } from 'angular2/core';
import { NgIf } from 'angular2/common';
import { ChapterService } from '../../services/chapter-service';
import { SelectCSSChapter } from '../css-select/css-select';
import { ChapterContent } from '../css-chapter-content/css-chapter-content';

@Component({
    selector: 'app-main',
    template: `
        <div class="app-container">

            <div class="header">
                <span class="css-book-title">CSS3: PUSHING THE LIMITS</span>
                <span class="header-title">Goals 2016</span>
            </div>

            <div class="body">
                <div class="left-margin">
                    <css-select></css-select>
                </div>

                <div class="center-margin">
                    <css-chapter-content></css-chapter-content>
                </div>

                <div class="right-margin"></div>
            </div>

            <div class="footer"></div>

        </div>
    `,
    styleUrls: ['../public/components/app-main/app-main.css'],
    directives: [ NgIf, SelectCSSChapter, ChapterContent ]
})
export class AppMain {

    constructor (private _chapterService: ChapterService) {}
}