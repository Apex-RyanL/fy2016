import { Component, Input } from 'angular2/core';
import { NgFor, NgIf } from 'angular2/common';
import { Concept } from '../css-concept/css-concept';
import { Section } from '../css-section/css-section';
import { TwistyBorder } from '../ui-twisty-border/ui-twisty-border';
import { ChapterService } from '../../services/chapter-service';

@Component({
    selector: 'css-chapter-content',
    template: `
        <link rel="stylesheet" type="text/css" [href]="pathToCss" >

        <ui-twisty-border
            *ngIf="activeCssChapter.sections.length > 0"
            [disabled]="true"
            >
            <css-section
                *ngFor="#section of activeCssChapter.sections;"
                [section]="section"
            >
                <css-concept
                    *ngFor="#concept of section.concepts;"
                    [concept]="concept"
                >
                    <div [innerHTML]="concept.html"></div>
                </css-concept>
            </css-section>
        </ui-twisty-border>
    `,
    styles: [`
        .chapter-title {
            margin: 5% 0 5% 5%;
            font-family: sans-serif;
        }
    `],
    directives: [ Concept, Section, NgFor, NgIf, TwistyBorder ]
})
export class ChapterContent {
    constructor (private _chapterService: ChapterService) {}

    private get pathToCss(): string { return '../public/configs/' + this.activeCssChapter.value + '/styles.css'; }
    private get activeCssChapter (): IChapter { return this._chapterService.activeCssChapter; }
}