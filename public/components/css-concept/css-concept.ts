import { Component, Input } from 'angular2/core';
import { TwistyBorder } from '../ui-twisty-border/ui-twisty-border';
import { Tidy } from '../../utils/tidyHTML';

@Component({
    selector: 'css-concept',
    template: `
        <ui-twisty-border [title]="concept.title">
            <span class="css-select-mode">
                <input type="button" value="Example" class="twisty-border"
                    (click)="display_mode = 'example'"
                    [ngClass]="{ selected: display_mode === 'example' }"
                />
                <input type="button" value="HTML" class="twisty-border"
                    (click)="display_mode = 'html'"
                    [ngClass]="{ selected: display_mode === 'html' }"
                />
                <input type="button" value="CSS" class="twisty-border"
                    (click)="display_mode = 'css'"
                    [ngClass]="{ selected: display_mode === 'css' }"
                />
            </span>
            <div class="css-concept-wrap">
                <ng-content *ngIf="display_mode === 'example'"></ng-content>
                <textarea disabled class="css-text twisty-border"
                    *ngIf="display_mode === 'html'"
                    placeholder="No HTML content."
                >{{tidyHTML(concept.html)}}</textarea>
                <textarea disabled class="css-text twisty-border"
                    *ngIf="display_mode === 'css'"
                    placeholder="No CSS content."
                >{{tidyHTML(concept.styles)}}</textarea>
            </div>
        </ui-twisty-border>
    `,
    styleUrls: [ '../public/components/css-concept/css-concept.css' ],
    directives: [ TwistyBorder ]
})
export class Concept {
    @Input ('concept') concept: IConcept;
    private tidy: Tidy;

    constructor () {
        this.tidy = new Tidy();
    }

    private display_mode: string = 'example';

    private tidyHTML (_html: string) {
        return _html; // this.tidy.tidy(_html);
    }
}