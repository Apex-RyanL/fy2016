import { Component, Input } from 'angular2/core';
import { NgIf, NgClass } from 'angular2/common';
import { TwistyBorder } from '../ui-twisty-border/ui-twisty-border';

@Component({
    selector: 'css-section',
    template: `

        <ui-twisty-border
            [title]="section.title"
        >
            <div class="css-section-wrap">
                <ng-content></ng-content>
            </div>
        </ui-twisty-border>
    `,
    styles: [`
        .css-section-wrap {
            position: relative;
            width: 96%;
            padding: 5% 2% 5% 2%;
        }
        .css-section-wrap:last-of-type {
            padding-bottom: 5%;
        }
    `],
    directives: [ NgIf, NgClass, TwistyBorder ]
})
export class Section {
    @Input ('section') section: ISection;
}