import { Component, ElementRef } from 'angular2/core';
import { NgFor, NgIf } from 'angular2/common';
import { ChapterService } from '../../services/chapter-service';
import { CHAPTERS } from '../../configs/chapters.config';

@Component({
    selector: 'css-select',
    templateUrl: '../public/components/css-select/css-select.html',
    styleUrls: [ '../public/components/css-select/css-select.css' ],
    directives: [ NgFor, NgIf ]
})
export class SelectCSSChapter {

    private dropdownList: Array<IChapter> = CHAPTERS.menu_dropdown;

    constructor (private _chapterService: ChapterService, private _elemRef: ElementRef) {}

    ngOnInit() {
        this._chapterService.activeCssChapter = this.dropdownList[0];
    }

    private onChangeOption (event: Event): void {
        var index: number = (<HTMLSelectElement> event.srcElement).selectedIndex;
        this._chapterService.activeCssChapter = this.dropdownList[index];
    }

    private get activeChapter (): IChapter { return this._chapterService.activeCssChapter; }
}