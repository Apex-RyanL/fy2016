import { Component, Input } from 'angular2/core';
import { NgIf, NgClass } from 'angular2/common';

@Component({
    selector: 'ui-twisty-border',
    template: `
        <div class="css-twisty-wrap">
            <span class="css-title" *ngIf="title !== ''">{{title}}</span>
            <div [ngClass]="{ 'css-twisty-border': !disabled }">
                <ng-content></ng-content>
            </div>
        </div>
    `,
    styleUrls: ['../public/components/ui-twisty-border/ui-twisty-border.css'],
    directives: [ NgIf, NgClass ]
})
export class TwistyBorder {
    @Input ('title') title: string = '';
    @Input ('disabled') disabled: boolean;
}