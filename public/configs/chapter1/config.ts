
/* Chapter 1: Advanced Selectors */
export const chapter1: IChapter = {
    value: 'chapter1',
    title: 'Chapter 1',
    description: 'Advanced Selectors',
    commonCSS: `
        div {
            font-family: monospace, sans-serif;
        }
        .twisty-border {
            border-top-left-radius: 7px;
            border-bottom-right-radius: 7px;
        }
        .twisty-border.blue {
            height: 200px;
            background: rgba(2, 137, 209, 0.5);
        }
        .twisty-border.violet {
            height: 200px;
            background: rgba(75, 27, 159,0.5);
        }
    `,
    sections: [
        {
            title: 'Child and Sibling Selectors',
            description: '',
            concepts: [
                {
                    title: 'Child Combinator',
                    html: `
                        <div class="s1-c1 twisty-border violet">
                            <span>Child Combinator</span>
                        </div>
                    `,
                    styles: `
                        .s1-c1 > span {
                            color: rgba(255,255,255,0.87);
                            font-family: sans-serif;
                            font-size: 32px;
                        }
                        .s1-c1 {
                            padding: 30px 5% 0 5%;
                        }
                    `
                },
                {
                    title: 'Adjacent Sibling Combinator',
                    html: `
                        <div class="s1-c2 twisty-border blue">
                            <h3>Sibling 1</h3>
                            <p>Sibling 2 (with border)</p>
                        </div>
                    `,
                    styles: `
                        .s1-c2 h3 + p {
                            border: 1px dashed rgba(255,255,255,0.87);
                            padding: 15px;
                        }
                        .s1-c2 {
                            padding: 10px 5% 0 5%;
                            color: rgba(255,255,255,0.87);
                            font-family: sans-serif;
                        }
                    `
                },
                {
                    title: 'General Sibling Combinator',
                    html: `
                        <div class="s1-c3 twisty-border violet">
                            <div class="header">Sibling Header</div>
                            <span>First General Sibling Combinator.</span>
                            <span>Second General Sibling Combinator.</span>
                            <span>Third General Sibling Combinator.</span>
                            <span>Fourth General Sibling Combinator.</span>
                            <span>Fifth General Sibling Combinator.</span>
                        </div>
                    `,
                    styles: `
                        .s1-c3 .header ~ span {
                            display: block;
                            padding: 5px 0;
                            border-bottom: 1px solid rgba(255,255,255,0.42);
                        }
                        .s1-c3 .header {
                            padding-bottom: 10px;
                            font-size: 24px;
                        }
                        .s1-c3 {
                            font-family: sans-serif;
                            color: rgba(255,255,255,0.87);
                            padding: 10px 5% 0 5%;
                        }
                    `
                }
            ]
        },
        {
            title: 'Attribute Selectors',
            concepts: [
                {
                    title: 'Selecting Based on the Existence of an HTML Attribute',
                    html: `
                        <div class="s2-c1 twisty-border blue">
                            <input disabled class="twisty-border" value="No Attribute." />
                            <input disabled class="twisty-border" type="text" value="With attribute." />
                        </div>
                    `,
                    styles: `
                        .s2-c1 input[type] {
                            color: rgba(255,0,0,0.87);
                        }
                        .s2-c1 input {
                            display: block;
                            width: 100px;
                            padding: 10px;
                            margin-top: 10px;
                            color: rgba(0,0,0,0.87);
                            font-family: sans-serif;
                            font-size: 16px;
                            text-align: center;
                        }
                        .s2-c1 {
                            padding: 20px 5%;
                        }
                    `
                },
                {
                    title: 'Selecting Based on the Exact Value of an HTML Attribute',
                    html: `
                        <div class="s2-c2 twisty-border violet">
                            <input class="twisty-border" disabled value="Default" />
                            <input class="twisty-border" disabled value="Red" />
                        </div>
                    `,
                    styles: `
                        .s2-c2 input[value="Red"] {
                            color: rgba(255,0,0,0.87);
                        }
                        .s2-c2 input {
                            display: block;
                            width: 100px;
                            padding: 10px;
                            margin-top: 10px;
                            color: rgba(0,0,0,0.87);
                            font-family: sans-serif;
                            font-size: 16px;
                            text-align: center;
                        }
                        .s2-c2 {
                            padding: 20px 5% 0 5%;
                        }
                    `
                },
                {
                    title: 'Selecting Based on Partial Contents of an HTML Attribute Value',
                    html: `
                        <div class="s2-c3 twisty-border blue">
                            <a target="_blank" href="http://lmgtfy.com">Let Me Google That For You</a>
                        </div>
                    `,
                    styles: `
                        .s2-c3 a[href*="lmgtfy"] {
                            color: rgba(255,255,255,0.87);
                            font-family: sans-serif;
                            font-size: 24px;
                        }
                        .s2-c3 {
                            padding: 20px 5% 0 5%;
                        }
                    `
                },
                {
                    title: 'Selecting Based on the Beginning of an HTML Attribute Value',
                    html: `
                        <div class="s2-c4 twisty-border violet">
                            <a target="_blank" href="http://www.google.com">Link to Google</a>
                        </div>
                    `,
                    styles: `
                        .s2-c4 a[href^="http"] {
                            color: rgba(255,255,255,0.87);
                            font-family: sans-serif;
                            font-size: 24px;
                        }
                        .s2-c4 {
                            padding: 20px 5% 0 5%;
                        }
                    `
                },
                {
                    title: 'Selecting Based on the End of an HTML Attribute Value',
                    html: `
                        <div class="s2-c5 twisty-border blue">
                            <a target="_blank" href="http://www.youtube.com">Link to Youtube</a>
                        </div>
                    `,
                    styles: `
                        .s2-c5 a[href$=".com"] {
                            color: rgba(255,255,255,0.87);
                            font-family: sans-serif;
                            font-size: 24px;
                        }
                        .s2-c5 {
                            padding: 20px 5% 0 5%;
                        }
                    `
                },
                {
                    title: 'Selecting Based on Space-Separated HTML Attribute Values',
                    html: `
                        <div class="s2-c6 twisty-border violet">
                            <span data-group="Reporter 1">John Smith</span>
                            <span data-group="Reporter 2">Bill Worth</span>
                            <span data-group="Reporter 1">Debbie Ray</span>
                        </div>
                    `,
                    styles: `
                        .s2-c6 span[data-group~="2"] {
                            font-size: 18px;
                            border-bottom: 1px dashed rgba(255,255,255,0.42);
                        }
                        .s2-c6 span {
                            display: block;
                            padding-top: 10px;
                            color: rgba(255,255,255,0.87);
                            font-family: sans-serif;
                            font-size: 24px;
                            border-bottom: 1px solid rgba(255,255,255,0.42);
                        }
                        .s2-c6 {
                            padding: 20px 5% 0 5%;
                        }
                    `
                }
            ]
        },
        {
            title: 'Pseudo-Classes',
            description: '',
            concepts: [
                {
                    title: 'Firsts and Lasts',
                    html: `
                        <div class="s3-c1 twisty-border blue">
                            <ul>
                                <li>Circle</li>
                                <li>Triangle</li>
                                <li>Square</li>
                                <li>Pentagon</li>
                                <li>Hexagon</li>
                            </ul>
                        </div>
                    `,
                    styles: `
                        .s3-c1 ul:only-child {
                            border: 1px solid rgba(255,255,255,0.42);
                            padding: 10px;
                        }
                        .s3-c1 ul li {
                            margin-left: 5%;
                            padding: 5px;
                        }
                        .s3-c1 ul li:first-of-type,
                        .s3-c1 ul li:last-of-type {
                            color: rgba(255,255,0,0.87);
                        }
                        .s3-c1 {
                            padding: 20px 5% 0 5%;
                            color: rgba(255,255,255,0.87);
                            font-family: sans-serif;
                        }
                    `
                },
                {
                    title: 'Nth Child Selectors',
                    html: `
                        <div class="s3-c2 twisty-border violet">
                            <ul>
                                <li>1 fish</li>
                                <li>2 fish</li>
                                <li>Red fish</li>
                                <li>Blue fish</li>
                            </ul>
                        </div>
                    `,
                    styles: `
                        .s3-c2 li:nth-child(3) {
                            color: rgba(255,0,0,0.87);
                        }
                        .s3-c2 li:nth-of-type(4) {
                            color: rgba(0,0,255,0.87);
                        }
                        .s3-c2 li {
                            padding-top: 5px;
                        }
                        .s3-c2 {
                            padding: 20px 5% 0 5%;
                            color: rgba(255,255,255,0.87);
                            font-family: sans-serif;
                        }
                    `
                },
                {
                    title: 'Taking Nth Child to the Next Level with Expressions',
                    html: `
                        <div class="s3-c3 twisty-border blue">
                            <span>a</span><span>b</span><span>c</span><span>d</span>
                            <span>e</span><span>f</span><span>g</span><span>h</span>
                            <span>i</span><span>j</span><span>k</span><span>l</span>
                            <span>m</span><span>n</span><span>o</span><span>p</span>
                            <span>q</span><span>r</span><span>s</span><span>t</span>
                            <span>u</span><span>v</span><span>w</span><span>x</span>
                            <span>y</span><span>z</span>
                        </div>
                    `,
                    styles: `
                        .s3-c3 span:nth-child(4n+5) {
                            margin-left: 5px;
                            padding-left: 10px;
                            border-left: 1px solid black;
                        }
                        .s3-c3 span {
                            display: inline-block;
                            padding: 5px;
                        }
                        .s3-c3 {
                            padding: 20px 5% 0 5%;
                            color: rgba(255,255,255,0.87);
                            font-size: 18px;
                            font-family: monospace, sans-serif;
                        }
                    `
                },
                {
                    title: 'Using Keywords with Nth Child',
                    html: `
                        <div class="s3-c4 twisty-border violet">
                            <span>even</span><span>odd</span><span>even</span><span>odd</span><span>even</span><span>odd</span><span>even</span><span>odd</span>
                        </div>
                    `,
                    styles: `
                        .s3-c4 span:nth-child(even) {
                            background-color: white;
                            color: black;
                        }
                        .s3-c4 span:nth-child(odd) {
                            background-color: black;
                            color: white;
                        }
                        .s3-c4 span {
                            padding: 0 5px 0 5px;                            
                        }
                        .s3-c4 {
                            padding: 20px 5% 0 5%;
                            color: rgba(255,255,255,0.87);
                            font-size: 18px;
                        }
                    `
                },
                {
                    title: 'Using Negative Numbers with Nth Child',
                    html: `
                        <div class="s3-c5 twisty-border blue">
                            <ol>
                                <li>apple</li>
                                <li>banana</li>
                                <li>cherry</li>
                                <li>doritos</li>
                                <li>eggplant</li>
                                <li>fruitloops</li>
                            </ol>
                        </div>
                    `,
                    styles: `
                        .s3-c5 ol li:nth-child(-n+2) {
                            background: lightgreen;
                        }
                        .s3-c5 ol li:nth-child(-n+5):nth-child(n+3) {
                            background: lightyellow;
                        }
                        .s3-c5 ol li:last-child {
                            background: pink;
                        }
                        .s3-c5 {
                            padding: 20px 5% 0 5%;
                            color: black;
                            font-size: 18px;
                        }
                    `
                }
            ]
        }
    ]
};