
/* Chapter 13: Getting Creative with Pseudo-elements */
export const chapter10: IChapter = {
    value: 'Chapter10',  // insert the appropriate selector tag in the main window
    title: 'Chapter 10',
    description: 'Getting Creative with Pseudo-elements',
    commonCSS: `
        @font-face {
            font-family: 'Sketch Block';
            src: url('fonts/sketch_block-webfont.eot');
            src: url('fonts/sketch_block-webfont.eot?#iefix') format('embedded-opentype'),
                 url('fonts/sketch_block-webfont.woff') format('woff'),
                 url('fonts/sketch_block-webfont.ttf') format('truetype'),
                 url('fonts/sketch_block-webfont.svg#SketchBlockBold') format('svg');
        }
        
        @font-face {
            font-family: 'Museo 700';
            src: url('fonts/Museo700-Regular-webfont.eot');
            src: url('fonts/Museo700-Regular-webfont.eot?#iefix') format('embedded-opentype'),
                 url('fonts/Museo700-Regular-webfont.woff') format('woff'),
                 url('fonts/Museo700-Regular-webfont.ttf') format('truetype'),
                 url('fonts/Museo700-Regular-webfont.svg#SketchBlockBold') format('svg');
        }
        
        @font-face {
            font-family: 'CallunaRegular';
            src: url('fonts/Calluna-Regular-webfont.eot');
            src: url('fonts/Calluna-Regular-webfont.eot?#iefix') format('embedded-opentype'),
                 url('fonts/Calluna-Regular-webfont.woff') format('woff'),
                 url('fonts/Calluna-Regular-webfont.ttf') format('truetype'),
                 url('fonts/Calluna-Regular-webfont.svg#CallunaRegular') format('svg');
        }
    
        div {
            font-family: monospace, sans-serif;
        }
        .twisty-border {
            border-top-left-radius: 7px;
            border-bottom-right-radius: 7px;
        }
        .twisty-border.blue {
            min-height: 200px;
            background: rgba(2, 137, 209, 0.5);
            color: black;
        }
        .twisty-border.violet {
            min-height: 200px;
            background: rgba(75, 27, 159,0.5);
            color: white;
        }
        .wrap {
            padding: 20px 5% 0 5%;
        }
    `,
    sections: [
        {
            title: 'What Is a Pseudo-element?',
            concepts: [
                {
                    title: 'Exploring the Current Range of Pseudo-elements',
                    html: `
                        <div class="s1-c1 wrap twisty-border violet">
                            <article class="container">
                                <div>This is the main headline</div>
                                <p>Lorem ipsum dolor sit amet, sem porttitor sem et in, augue risus nec in in. Imperdiet arcu aliquam, orci dictum in, id lorem viverra nunc arcu, commodo nec leo enim. Porttitor suscipit pulvinar nunc est, porttitor non tortor a amet eros, commodo tristique nam hac, tincidunt commodo, nonummy aenean bibendum suspendisse magna. Non ultrices laoreet elit, ullamcorper enim velit ac neque mauris ac, mollis nonummy posuere vivamus metus massa neque, nulla taciti parturient dui id.</p>
                                <p>Ac euismod est ad ligula, congue itaque pede urna in sed, ullam rhoncus nec velit sed integer eros, quis sed et cras mi eu. Enim vel elit sit, arcu ornare nunc, sollicitudin litora mi ipsum vulputate ornare dignissim, nunc ipsum dolor sed. Urna conubia velit potenti arcu, cras tortor, vestibulum massa ornare nam duis luctus, in ac purus sem turpis adipiscing. Augue cum vitae, massa et. Fusce nisl, vehicula dictum ac senectus et fermentum, felis praesent vitae, veniam montes sit dictum.</p>
                                <p>Posuere duis mauris, justo purus purus sed dui erat quis, non semper mus. Curabitur justo, ante a magna in consequat, sit scelerisque dui, sed ut a. Vestibulum nulla, venenatis tristique, vulputate luctus amet integer vel.</p>
                            </article>
                        </div>
                    `,
                    styles: `
                        .s1-c1 {
                            padding-bottom: 20px;
                        }
                        .s1-c1 .container {
                            padding: 10px;
                            text-align: justify;
                            columns: 2;
                            color: #feffb2;
                            background: #333;
                        }
                        .s1-c1 .container div:first-of-type {
                            column-span: all;
                            text-align: justify;
                            font-size: 48px;
                            margin: 50px 0;
                        }
                        .s1-c1 .container p:first-of-type {
                            color: #fff;
                        }
                        .s1-c1 .container p:first-of-type:first-line {
                            font-weight: bold;
                            font-size: 1.3em;
                        }
                        .s1-c1 .container p:first-of-type:first-letter {
                            float: left;
                            padding: 0.2em;
                            margin-right: 0.1em;
                            border: 2px dotted #ccc;
                            border-radius: 10px;
                            font-size: 3em;
                        }
                    `
                },
                {
                    title: 'Getting Creative with Type-based Pseudo-elements',
                    html: `
                        <div class="s1-c2 wrap twisty-border blue">
                            <h2>Preview example:</h2>
                            <div class="container">
                                <a href="http://math.mercyhurst.edu/~griff/courses/mis280/Resources/CSS3_Pushing/ch13demos/1302-creative-typography.html" target="_blank">imagination</a>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s1-c2 .container {
                            width: 240px;
                            background: #333;
                        }
                        .s1-c2 .container a {
                            padding: 20px 0;
                            text-align: center;
                            font-size: 36px;
                            color: #feffb2;
                        }
                    `
                }
            ]
        },
        {
            title: 'Creating Scalable Icons and Shapes',
            concepts: [
                {
                    title: 'Creating a Speech Bubble',
                    html: `
                        <div class="s2-c1 wrap twisty-border violet">
                            <blockquote class="speech-bubble">"This is a quote"</blockquote>
                        </div>
                    `,
                    styles: `
                        .s2-c1 {
                            padding-bottom: 30px;
                        }
                        .s2-c1 .speech-bubble {
                            display: inline-block;
                            position: relative;
                            padding: 80px;
                            margin-left: 100px;
                            border-radius: 50%;
                            color: black;
                            background: #fff;
                        }
                        .s2-c1 .speech-bubble:before {
                            content: "";
                            position: absolute;
                            width: 50px;
                            height: 50px;
                            bottom: -15px;
                            left: -20px;
                            border-radius: 50%;
                            border-left: 40px solid transparent;
                            border-right: 40px solid #fff;
                        }
                    `
                },
                {
                    title: 'Creating an OK Icon',
                    html: `
                        <div class="s2-c2 wrap twisty-border blue">
                            <div>
                                <a class="ok">OK</a>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s2-c2 div {
                            padding: 75px 0;
                            background: #333;
                        }
                        .s2-c2 .ok {
                            position: relative;
                            padding-left: 30px;
                            margin-left: 150px;
                            color: white;
                        }
                        .s2-c2 .ok:before {
                            content: "";
                            position: absolute;
                            left: 3px;
                            width: 15px;
                            height: 7px;
                            border-left: 4px solid white;
                            border-bottom: 4px solid white;
                            transform: rotate(-45deg);
                        }
                    `
                },
                {
                    title: 'Creating a Print Icon',
                    html: `
                        <div class="s2-c3 wrap twisty-border violet">
                            <div>
                                <a class="print">Print</a>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s2-c3 div {
                            padding: 75px 0;
                            background: #333;
                        }
                        .s2-c3 .print {
                            position: relative;
                            padding-left: 30px;
                            margin-left: 150px;
                            
                        }
                        .s2-c3 .print:before {
                            content: "";
                            position: absolute;
                            left: 7px;
                            top: -2px;
                            width: 9px;
                            height: 24px;
                            background: white;
                        }
                        .s2-c3 .print:after {
                            content: "";
                            position: absolute;
                            left: 0;
                            top: 5px;
                            width: 19px;
                            height: 12px;
                            border-left: 2px solid #ddd;
                            border-right: 2px solid #ddd;
                            background: #777;
                        }
                    `
                },
                {
                    title: 'Creating a Save Icon',
                    html: `
                        <div class="s2-c4 wrap twisty-border blue">
                            <div>
                                <a class="save">Save</a>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s2-c4 div {
                            padding: 75px 0;
                            color: white;
                            background: #333;
                        }
                        .s2-c4 .save {
                            position: relative;
                            padding-left: 30px;
                            margin-left: 150px;
                        }
                        .s2-c4 .save:before {
                            content: "";
                            position: absolute;
                            left: 0;
                            width: 12px;
                            height: 7px;
                            background: #fff;
                            border-bottom-left-radius: 4px;
                            border: 4px solid #125ea5;
                            border-top: 1px solid #125ea5;
                            border-bottom: 11px solid #125ea5;
                        }
                        .s2-c4 .save:after {
                            content: "";
                            position: absolute;
                            left: 5px;
                            bottom: 2px;
                            width: 3px;
                            height: 4px;
                            border-left: 2px solid #ccc;
                            border-top: 1px solid #ccc;
                            border-right: 5px solid #ccc;
                        }
                    `
                }
            ]
        }
    ]
};