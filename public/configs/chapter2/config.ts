
/* Chapter 2: New Tools for Text */
export const chapter2: IChapter = {
    value: 'Chapter2',  // insert the appropriate selector tag in the main window
    title: 'Chapter 2',
    description: 'New Tools for Text',
    commonCSS: `
        div {
            font-family: monospace, sans-serif;
        }
        .twisty-border {
            border-top-left-radius: 7px;
            border-bottom-right-radius: 7px;
        }
        .twisty-border.blue {
            height: 200px;
            background: rgba(2, 137, 209, 0.5);
            color: black;
        }
        .twisty-border.violet {
            height: 200px;
            background: rgba(75, 27, 159,0.5);
            color: white;
        }
        .wrap {
            padding: 20px 5% 0 5%;
        }
    `,
    sections: [
        {
            title: 'Perfecting Your Type',
            concepts: [
                {
                    title: 'Ligatures',
                    html: `
                        <div class="s1-c1 wrap twisty-border violet">
                            <h2 class="title">Setting the font variant ligatures:</h2>
                            <div>-webkit-font-variant-ligatures: common-ligatures;</div>
                            <div>-webkit-font-variant-ligatures: no-common-ligatures;</div>
                            <div>-webkit-font-variant-ligatures: discretionary-ligatures;</div>
                            <div>-webkit-font-variant-ligatures: no-discretionary-ligatures;</div>
                            <div>-webkit-font-variant-ligatures: historical-ligatures;</div>
                            <div>-webkit-font-variant-ligatures: no-historical-ligatures;</div>
                            <div>-webkit-font-variant-ligatures: contextual;</div>
                            <div>-webkit-font-variant-ligatures: no-contextual;</div>
                        </div>
                    `,
                    styles: `
                        .s1-c1 div {
                            font-family: serif;
                            font-size: 12px;
                        }
                        .s1-c1 div:first-child {
                            -webkit-font-variant-ligatures: common-ligatures;
                        }
                        .s1-c1 div:nth-child(2) {
                            -webkit-font-variant-ligatures: no-common-ligatures;
                        }
                        .s1-c1 div:nth-child(3) {
                            -webkit-font-variant-ligatures: discretionary-ligatures;
                        }
                        .s1-c1 div:nth-child(4) {
                            -webkit-font-variant-ligatures: no-discretionary-ligatures;
                        }
                        .s1-c1 div:nth-child(5) {
                            -webkit-font-variant-ligatures: historical-ligatures;
                        }
                        .s1-c1 div:nth-child(6) {
                            -webkit-font-variant-ligatures: no-historical-ligatures;
                        }
                        .s1-c1 div:nth-child(7) {
                            -webkit-font-variant-ligatures: contextual;
                        }
                        .s1-c1 div:nth-child(8) {
                            -webkit-font-variant-ligatures: no-contextual;
                        }
                    `
                },
                {
                    title: 'Kerning',
                    html: `
                        <div class="s1-c2 wrap twisty-border blue">
                            <h2>Setting the font kerning:</h2>
                            <div>font-kerning: normal;</div>
                            <div>font-kerning: none;</div>
                            <div>font-kerning: auto;</div>
                        </div>
                    `,
                    styles: `
                        .s1-c2 div:first-child {
                            font-kerning: normal;
                        }
                        .s1-c2 div:nth-child(2) {
                            font-kerning: none;
                        }
                        .s1-c2 div:nth-child(3) {
                            font-kerning: auto;
                        }
                    `
                },
                {
                    title: 'Borrowing from SVG',
                    html: `
                        <div class="s1-c3 wrap twisty-border violet">
                            <h2>WAR on Waffles - no styles</h2>
                            <div>WAR on Waffles - text-rendering: optimizeLegibility;</div>
                            <div>WAR on Waffles - text-rendering: optimizePrecision;</div>
                            <div>WAR on Waffles - text-rendering: optimizeSpeed;</div>
                        </div>
                    `,
                    styles: `
                        .s1-c3 div:first-child {
                            text-rendering: optimizeLegibility;
                            font-size: 20px;
                        }
                        .s1-c3 div:nth-child(2) {
                            text-rendering: optimizePrecision;
                            font-size: 16px;
                        }
                        .s1-c3 div:nth-child(3) {
                            text-rendering: optimizeSpeed;
                            font-size: 12px;
                        }
                    `
                },
                {
                    title: 'Maintaining Legibility with Aspect Values',
                    html: `
                        <div class="s1-c4 twisty-border blue">
                            <p>
                                <span>X</span>
                                <span class="">X</span>
                            </p>
                        </div>
                    `,
                    styles: `
                        .s1-c4 {
                            padding-left: 5%;
                        }
                        .s1-c4 p {
                            font: 150px Arial;
                            margin: 0;
                        }
                        .s1-c4 span {
                            border: 1px dashed yellow;
                        }
                        .s1-c4 .adjust {
                            font-size-adjust: 0.52;
                        }
                    `
                }
            ]
        },
        {
            title: 'More Control with More New Toys',
            concepts: [
                {
                    title: 'Font Stretch',
                    html: `
                        <div class="s2-c1 wrap twisty-border violet">
                            <h2>Setting the font stretch:</h2>
                            <div>font-stretch: ultra-condensed;</div>
                            <div>font-stretch: extra-condensed;</div>
                            <div>font-stretch: condensed;</div>
                            <div>font-stretch: semi-condensed;</div>
                            <div>font-stretch: normal;</div>
                            <div>font-stretch: semi-expanded;</div>
                            <div>font-stretch: expanded;</div>
                            <div>font-stretch: extra-expanded;</div>
                            <div>font-stretch: ultra-expanded;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c1 div {
                            font-size: 10px;
                        }
                        .s2-c1 div:first-child {
                            font-stretch: ultra-condensed;
                        }
                        .s2-c1 div:nth-child(2) {
                            font-stretch: extra-condensed;
                        }
                        .s2-c1 div:nth-child(3) {
                            font-stretch: condensed;
                        }
                        .s2-c1 div:nth-child(4) {
                            font-stretch: semi-condensed;
                        }
                        .s2-c1 div:nth-child(5) {
                            font-stretch: normal;
                        }
                        .s2-c1 div:nth-child(6) {
                            font-stretch: semi-expanded;
                        }
                        .s2-c1 div:nth-child(7) {
                            font-stretch: expanded;
                        }
                        .s2-c1 div:nth-child(8) {
                            font-stretch: extra-expanded;
                        }
                        .s2-c1 div:nth-child(9) {
                            font-stretch: ultra-expanded;
                        }
                    `
                },
                {
                    title: 'Synthetic Font Styling',
                    html: `
                        <div class="s2-c2 wrap twisty-border blue">
                            <h2>Setting the font synthesis:</h2>
                            <div>font-synthesis: none;</div>
                            <div>font-synthesis: weight;</div>
                            <div>font-synthesis: style;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c2 div {
                            font-weight: bold;
                            font-style: italic;
                        }
                        .s2-c2 div:first-child {
                            font-synthesis: none;
                        }
                        .s2-c2 div:nth-child(2) {
                            font-synthesis: weight;
                        }
                        .s2-c2 div:nth-child(3) {
                            font-synthesis: style;
                        }
                    `
                },
                {
                    title: 'By the Numbers',
                    html: `
                        <div class="s2-c3 wrap twisty-border violet">
                            <h2>Setting the font variant numeric:</h2>
                            <div>font-variant-numeric: old-style-nums;</div>
                            <div>0123456789</div>
                            <div>font-variant-numeric: lining-nums;</div>
                            <div>0123456789</div>
                            <div>font-variant-numeric: proportional-nums;</div>
                            <div>0123456789</div>
                            <div>font-variant-numeric: tabular-nums;</div>
                            <div>0123456789</div>
                            <div>font-variant-numeric: stacked-fractions;</div>
                            <div>1 1/2</div>
                            <div>font-variant-numeric: diagonal-fractions;</div>
                            <div>2 2/3</div>
                        </div>
                    `,
                    styles: `
                        .s2-c3 {
                            padding-bottom: 60px;
                        }
                        .s2-c3 div {
                            font-size: 10px;
                        }
                        .s2-c3 div:nth-child(odd) {
                            margin: 0 0 5px 10px;
                        }
                        .s2-c3 div:nth-child(2) {
                            font-variant-numeric: old-style-nums;
                        }
                        .s2-c3 div:nth-child(4) {
                            font-variant-numeric: lining-nums;
                        }
                        .s2-c3 div:nth-child(6) {
                            font-variant-numeric: proportional-nums;
                        }
                        .s2-c3 div:nth-child(8) {
                            font-variant-numeric: tabular-nums;
                        }
                        .s2-c3 div:nth-child(10) {
                            font-variant-numeric: stacked-fractions;
                        }
                        .s2-c3 div:nth-child(12) {
                            font-variant-numeric: diagonal-fractions;
                        }
                    `
                },
                {
                    title: 'Hyphenation',
                    html: `
                        <div class="s2-c4 wrap twisty-border blue">
                            <h2>Setting the hyphen:</h2>
                            <div>hyphen: manual;</div>
                            <div>hyphen: auto;</div>
                            <div>hyphen: none;</div>
                        </div>
                    `,
                    styles: `
                        
                    `
                }
            ]
        },
        {
            title: 'Looking Good Is Half the Battle',
            concepts: [
                {
                    title: 'Text Decoration',
                    html: `
                        <div class="s3-c1 wrap twisty-border violet">
                            <table>
                                <thead>
                                    <tr>
                                        <th>text-decoration-line:</th>
                                        <th>text-decoration-color:</th>
                                        <th>text-decoration-style:</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>none;</td>
                                        <td>#hexcolor</td>
                                        <td>solid;</td>
                                    </tr>
                                    <tr>
                                        <td>underline;</td>
                                        <td>[any color]</td>
                                        <td>double;</td>
                                    </tr>
                                    <tr>
                                        <td>overline;</td>
                                        <td></td>
                                        <td>dotted;</td>
                                    </tr>
                                    <tr>
                                        <td>line-through;</td>
                                        <td></td>
                                        <td>dashed;</td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>wavy;</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    `,
                    styles: `
                        .s3-c1 table {
                            width: 90%;
                            background: white;
                            color: black;
                            text-align: center;
                        }
                        .s3-c1 table th {
                            border-bottom: 1px dashed black;
                        }
                        .s3-c1 table th:nth-child(1),
                        .s3-c1 table th:nth-child(2),
                        .s3-c1 table td:nth-child(1),
                        .s3-c1 table td:nth-child(2) {
                            border-right: 1px dashed black;
                        }
                    `
                },
                {
                    title: 'Masking Background Images Over Text',
                    html: `
                        <div class="s3-c2 wrap twisty-border blue">
                            <h1>The Big Headline!</h1>
                        </div>
                    `,
                    styles: `
                        .s3-c2 h1 {
                            font: 80px Bebas;
                            background: url(http://placekitten.com/g/750/200);
                            -webkit-background-clip: text;
                            color: #000;
                            -webkit-text-fill-color: transparent;
                            background-size: 750px 100px;
                        }
                    `
                },
                {
                    title: 'Text Shadow',
                    html: `
                        <div class="s3-c3 wrap twisty-border violet">
                            <h1>3d Headline!!</h1>
                        </div>
                    `,
                    styles: `
                        .s3-c3 h1 {
                            text-shadow:    0 1px 0 #ccc,
                                            0 2px 0 #c5c5c5,
                                            0 3px 0 #bbb,
                                            0 4px 0 #b5b5b5,
                                            0 5px 0 #aaa,
                                            0 6px 0 #a5a5a5,
                                            0 7px 3px rgba(0, 0, 0, 0.3),
                                            0 10px 10px rgba(0, 0, 0, 0.2),
                                            0 10px 20px rgba(0, 0, 0, 0.15),
                                            0 20px 20px rgba(0, 0, 0, 0.1);
                        }
                    `
                },
                {
                    title: 'Creating Outer Glow',
                    html: `
                        <div class="s3-c4 wrap twisty-border blue">
                            <h1>Headline With Glow!</h1>
                            <h1>Headline With Blur</h1>
                        </div>
                    `,
                    styles: `
                        .s3-c4 h1 {
                            font-size: 45px;
                            text-shadow:    0 0 10px #fff,
                                            0 0 20px rgba(255, 255, 255, 0.3),
                                            0 0 30px rgba(255, 33, 207, 1),
                                            0 0 40px rgba(255, 33, 207, 0.8),
                                            0 0 60px rgba(255, 33, 207, 0.5),
                                            0 0 100px rgba(255, 33, 207, 0.3);
                        }
                        .s3-c4 h1:first-child {
                            color: rgba(255, 255, 255, 0.8);
                        }
                        .s3-c4 h1:last-child {
                            color: transparent;
                        }
                    `
                }
            ]
        }
    ]
};