
/* Chapter 3: New Tools for Backgrounds and Borders */
export const chapter3: IChapter = {
    value: 'Chapter3',  // insert the appropriate selector tag in the main window
    title: 'Chapter 3',
    description: 'New Tools for Backgrounds and Borders',
    commonCSS: `
        div {
            font-family: monospace, sans-serif;
        }
        .twisty-border {
            border-top-left-radius: 7px;
            border-bottom-right-radius: 7px;
        }
        .twisty-border.blue {
            height: 200px;
            background: rgba(2, 137, 209, 0.5);
            color: black;
        }
        .twisty-border.violet {
            height: 200px;
            background: rgba(75, 27, 159,0.5);
            color: white;
        }
        .wrap {
            padding: 20px 5% 0 5%;
        }
    `,
    sections: [
        {
            title: 'More Control with CSS3 Backgrounds',
            concepts: [
                {
                    title: 'Background Clip and Background Origin',
                    html: `
                        <div class="s1-c1 wrap twisty-border violet">
                            <h2>Setting the background clip:</h2>
                            <div><p>background-clip: border-box;</p></div>
                            <div><p>background-clip: padding-box;</p></div>
                            <div><p>background-clip: content-box;</p></div>
                        </div>
                    `,
                    styles: `
                        .s1-c1 {
                            height: 300px;
                        }
                        .s1-c1 div {
                            width: 200px;
                            height: 50px;
                            display: inline-block;
                            margin-left: 10px;
                            padding: 10px;
                            background: rgba(0, 0, 0, 0.95);
                            border: 10px solid rgba(0, 0, 0, 0.6);
                        }
                        .s1-c1 div:first-child {
                            background-clip: border-box;
                        }
                        .s1-c1 div:nth-child(2) {
                            background-clip: padding-box;
                        }
                        .s1-c1 div:nth-child(3) {
                            background-clip: content-box;
                        }
                    `
                },
                {
                    title: 'Background Size',
                    html: `
                        <div class="s1-c2 wrap twisty-border blue">
                            <h2>Setting the background size:</h2>
                            <div>background-size: contains;</div>
                            <div>background-size: cover;</div>
                        </div>
                    `,
                    styles: `
                        .s1-c2 div {
                            display: inline-block;
                            width: 200px;
                            height: 100px;
                            background: url(http://placekitten.com/g/200/150);
                        }
                        .s1-c2 div:nth-child(1) {
                            background-size: contains;
                        }
                        .s1-c2 div:nth-child(2) {
                            background-size: cover;
                        }
                    `
                },
                {
                    title: 'Understanding the Background Shorthand',
                    html: `
                        <div class="s1-c3 wrap twisty-border violet">
                            <h2>Setting background WITHOUT shorthand:</h2>
                            <div>background-image: url(http://placekitten.com/g/200/150);</div>
                            <div>background-position: 0 50px;</div>
                            <div>background-size: cover;</div>
                            <div>background-repeat: no-repeat;</div>
                            <div>background-attachment: fixed;</div>
                            <div>background-origin: border-box;</div>
                            <div>background-clip: padding-box;</div>
                            <div>background-color: #333;</div>
                            <h2>Setting background WITH shorthand:</h2>
                            <div>background: url(http://placekitten.com/g/200/150) 0 50px / cover no-repeat fixed border-box padding-box #333;</div>
                        </div>
                    `,
                    styles: `
                        .s1-c3 {
                            padding-bottom: 100px;
                        }
                        .s1-c3 div {
                            font-size: 11px;
                        }
                    `
                }
            ]
        },
        {
            title: 'Multiple Backgrounds',
            concepts: [
                {
                    title: 'The Syntax for Multiple Backgrounds',
                    html: `
                        <div class="s2-c1 wrap twisty-border blue">
                            <div></div>
                        </div>
                    `,
                    styles: `
                        .s2-c1 div {
                            width: 256px;
                            height: 256px;
                            background: url(https://gigglefiber.com/wp-content/uploads/revslider/front-slider/cloud_PNG28.png) 20px 0 / cover,
                                        url(http://placekitten.com/g/256/156) 0 0 / cover;
                        }
                        .s2-c1 {
                            padding-bottom: 76px;
                        }
                    `
                },
                {
                    title: 'Animating Multiple Backgrounds',
                    html: `
                        <div class="s2-c2 wrap twisty-border violet">
                            <div></div>
                        </div>
                    `,
                    styles: `
                        .s2-c2 {
                            padding-bottom: 76px;
                        }
                        .s2-c2 div {
                            width: 256px;
                            height: 256px;
                            border-radius: 50%;
                            background: url(https://gigglefiber.com/wp-content/uploads/revslider/front-slider/cloud_PNG28.png) 20px 0 / cover,
                                        url(http://placekitten.com/g/256/156) 0 0 / cover;
                            transition: 1s;
                        }
                        .s2-c2 div:hover {
                            background-position: 100px -50px, 0 0
                        }
                    `
                }
            ]
        },
        {
            title: 'New Tools for Borders',
            concepts: [
                {
                    title: 'Border Image',
                    html: `
                        <div class="s3-c1 wrap twisty-border blue">
                            <h2>Setting border image:</h2>
                            <div>border-image-source:</div>
                            <div>url(...);</div>
                            <div>border-image-slice:</div>
                            <div>10 20 10 20; 45 fill;</div>
                            <div>border-image-width:</div>
                            <div>45 45 45 45; auto;</div>
                            <div>border-image-repeat:</div>
                            <div>stretch; repeat; round; space;</div>
                        </div>
                    `,
                    styles: `
                        .s3-c1 {
                            padding-bottom: 20px;
                        }
                        .s3-c1 div {
                            font-size: 12px;
                        }
                        .s3-c1 div:nth-child(odd) {
                            margin-left: 20px;
                            margin-bottom: 5px;
                        }
                    `
                },
                {
                    title: 'Into the Future with Border Corner Shape',
                    html: `
                        <div class="s3-c2 wrap twisty-border violet">
                            <h2>Setting the border corner shape:</h2>
                            <div class="_1">
                                <div>border-corner-shape: scoop;</div>
                                <div>border-corner-shape: notch;</div>
                            </div>
                            <div class="_2">
                                <div>border-corner-shape: bevel;</div>
                                <div>border-corner-shape: curve; (default)</div>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s3-c2 {
                            padding-bottom: 110px;
                        }
                        .s3-c2 ._2 {
                            margin-top: 20px;
                        }
                        .s3-c2 ._1 div,
                        .s3-c2 ._2 div {
                            display: inline-block;
                            width: 200px;
                            height: 60px;
                            border-radius: 20px;
                            padding: 20px;
                            border-color: rgba(0, 0, 0, 0.5);
                            background: rgba(2, 137, 209, 0.5);
                        }
                        .s3-c2 ._1 div:first-child {
                            border-corner-shape: scoop;
                        }
                        .s3-c2 ._1 div:last-child {
                            border-corner-shape: notch;
                        }
                        .s3-c2 ._2 div:first-child {
                            border-corner-shape: bevel;
                        }
                        .s3-c2 ._2 div:last-child {
                            border-corner-shape: curve;
                        }
                    `
                }
            ]
        },
        {
            title: 'Gradients',
            concepts: [
                {
                    title: 'Linear Gradients',
                    html: `
                        <div class="s4-c1 wrap twisty-border blue">
                            <div></div>
                        </div>
                    `,
                    styles: `
                        .s4-c1 {
                            padding-bottom: 76px;
                        }
                        .s4-c1 div {
                            width: 250px;
                            height: 250px;
                            background: linear-gradient(to top, #fff, transparent) bottom fixed,
                                        url(http://placekitten.com/g/250/250) 0 0 / cover;
                        }
                    `
                },
                {
                    title: 'Radial Gradients',
                    html: `
                        <div class="s4-c2 wrap twisty-border violet">
                            <div></div>
                        </div>
                    `,
                    styles: `
                        .s4-c2 {
                            padding-bottom: 76px;
                        }
                        .s4-c2 div {
                            width: 250px;
                            height: 250px;
                            background: radial-gradient(transparent, black 90%) fixed,
                                        radial-gradient(rgba(250, 250, 250, 0.3), rgba(0, 0, 0, 0.3) 90%) fixed,
                                        url(http://placekitten.com/g/250/250);
                        }
                    `
                }
            ]
        }
    ]
};