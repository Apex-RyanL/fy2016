
/* Chapter 4: Into the Browser with CSS3 Filters and Blending Modes */
export const chapter4: IChapter = {
    value: 'Chapter4',  // insert the appropriate selector tag in the main window
    title: 'Chapter 4',
    description: 'Into the Browser with CSS3 Filters and Blending Modes',
    commonCSS: `
        div {
            font-family: monospace, sans-serif;
        }
        .twisty-border {
            border-top-left-radius: 7px;
            border-bottom-right-radius: 7px;
        }
        .twisty-border.blue {
            height: 200px;
            background: rgba(2, 137, 209, 0.5);
            color: black;
        }
        .twisty-border.violet {
            height: 200px;
            background: rgba(75, 27, 159,0.5);
            color: white;
        }
        .wrap {
            padding: 20px 5% 0 5%;
        }
    `,
    sections: [
        {
            title: 'CSS3 Filters',
            concepts: [
                {
                    title: 'Grayscale',
                    html: `
                        <div class="s1-c1 wrap twisty-border violet">
                            <h2>Setting filter -> grayscale:</h2>
                            <div>filter: grayscale(0.25);</div>
                            <div>filter: grayscale(50%);</div>
                            <div>filter: grayscale(1);</div>
                        </div>
                    `,
                    styles: `
                        .s1-c1 {
                            padding-bottom: 95px;
                        }
                        .s1-c1 div {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            background: url(../../images/cat_350x50.png);
                            color: white;
                        }
                        .s1-c1 div:nth-child(2) {
                            filter: grayscale(0.25);
                        }
                        .s1-c1 div:nth-child(3) {
                            filter: grayscale(50%);
                        }
                        .s1-c1 div:last-child {
                            filter: grayscale(1);
                        }
                    `
                },
                {
                    title: 'Brightness',
                    html: `
                        <div class="s1-c2 wrap twisty-border blue">
                            <h2>Setting filter -> brightness:</h2>
                            <div>filter: brightness(0.25);</div>
                            <div>filter: brightness(50%);</div>
                            <div>filter: brightness(1);</div>
                        </div>
                    `,
                    styles: `
                        .s1-c2 {
                            padding-bottom: 95px;
                        }
                        .s1-c2 div {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            background: url(../../images/cat_350x50.png);
                            color: white;
                        }
                        .s1-c2 div:nth-child(2) {
                            filter: brightness(0.25);
                        }
                        .s1-c2 div:nth-child(3) {
                            filter: brightness(50%);
                        }
                        .s1-c2 div:last-child {
                            filter: brightness(1);
                        }
                    `
                },
                {
                    title: 'Contrast',
                    html: `
                        <div class="s1-c3 wrap twisty-border violet">
                            <h2>Setting filter -> contrast:</h2>
                            <div>filter: contrast(0.25);</div>
                            <div>filter: contrast(50%);</div>
                            <div>filter: contrast(1);</div>
                        </div>
                    `,
                    styles: `
                        .s1-c3 {
                            padding-bottom: 95px;
                        }
                        .s1-c3 div {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            background: url(../../images/cat_350x50.png);
                            color: white;
                        }
                        .s1-c3 div:nth-child(2) {
                            filter: contrast(0.25);
                        }
                        .s1-c3 div:nth-child(3) {
                            filter: contrast(50%);
                        }
                        .s1-c3 div:last-child {
                            filter: contrast(1);
                        }
                    `
                },
                {
                    title: 'Saturate',
                    html: `
                        <div class="s1-c4 wrap twisty-border blue">
                            <h2>Setting filter -> saturate:</h2>
                            <div>filter: saturate(0.25);</div>
                            <div>filter: saturate(50%);</div>
                            <div>filter: saturate(1);</div>
                        </div>
                    `,
                    styles: `
                        .s1-c4 {
                            padding-bottom: 95px;
                        }
                        .s1-c4 div {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            background: url(../../images/cat_350x50.png);
                            color: white;
                        }
                        .s1-c4 div:nth-child(2) {
                            filter: saturate(0.25);
                        }
                        .s1-c4 div:nth-child(3) {
                            filter: saturate(50%);
                        }
                        .s1-c4 div:last-child {
                            filter: saturate(1);
                        }
                    `
                },
                {
                    title: 'Sepia',
                    html: `
                        <div class="s1-c5 wrap twisty-border violet">
                            <h2>Setting filter -> sepia:</h2>
                            <div>filter: sepia(0.25);</div>
                            <div>filter: sepia(50%);</div>
                            <div>filter: sepia(1);</div>
                        </div>
                    `,
                    styles: `
                        .s1-c5 {
                            padding-bottom: 95px;
                        }
                        .s1-c5 div {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            background: url(../../images/cat_350x50.png);
                            color: white;
                        }
                        .s1-c5 div:nth-child(2) {
                            filter: sepia(0.25);
                        }
                        .s1-c5 div:nth-child(3) {
                            filter: sepia(50%);
                        }
                        .s1-c5 div:last-child {
                            filter: sepia(1);
                        }
                    `
                },
                {
                    title: 'Hue-Rotate',
                    html: `
                        <div class="s1-c6 wrap twisty-border blue">
                            <h2>Setting filter -> hue-rotate:</h2>
                            <div>filter: hue-rotate(0.25);</div>
                            <div>filter: hue-rotate(50%);</div>
                            <div>filter: hue-rotate(1);</div>
                        </div>
                    `,
                    styles: `
                        .s1-c6 {
                            padding-bottom: 95px;
                        }
                        .s1-c6 div {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            background: url(../../images/cat_350x50.png);
                            color: white;
                        }
                        .s1-c6 div:nth-child(2) {
                            filter: saturate(0.25);
                        }
                        .s1-c6 div:nth-child(3) {
                            filter: saturate(50%);
                        }
                        .s1-c6 div:last-child {
                            filter: saturate(1);
                        }
                    `
                },
                {
                    title: 'Invert',
                    html: `
                        <div class="s1-c7 wrap twisty-border violet">
                            <h2>Setting filter -> invert:</h2>
                            <div>filter: invert(0.25);</div>
                            <div>filter: invert(50%);</div>
                            <div>filter: invert(1);</div>
                        </div>
                    `,
                    styles: `
                        .s1-c7 {
                            padding-bottom: 95px;
                        }
                        .s1-c7 div {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            background: url(../../images/cat_350x50.png);
                            color: white;
                        }
                        .s1-c7 div:nth-child(2) {
                            filter: invert(0.25);
                        }
                        .s1-c7 div:nth-child(3) {
                            filter: invert(50%);
                        }
                        .s1-c7 div:last-child {
                            filter: invert(1);
                        }
                    `
                },
                {
                    title: 'Opacity',
                    html: `
                        <div class="s1-c8 wrap twisty-border blue">
                            <h2>Setting filter -> opacity:</h2>
                            <div>filter: opacity(0.25);</div>
                            <div>filter: opacity(50%);</div>
                            <div>filter: opacity(1);</div>
                        </div>
                    `,
                    styles: `
                        .s1-c8 {
                            padding-bottom: 95px;
                        }
                        .s1-c8 div {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            background: url(../../images/cat_350x50.png);
                            color: white;
                        }
                        .s1-c8 div:nth-child(2) {
                            filter: opacity(0.25);
                        }
                        .s1-c8 div:nth-child(3) {
                            filter: opacity(50%);
                        }
                        .s1-c8 div:last-child {
                            filter: opacity(1);
                        }
                    `
                },
                {
                    title: 'Drop Shadow',
                    html: `
                        <div class="s1-c9 wrap twisty-border violet">
                            <h2>Setting filter -> drop-shadow:</h2>
                            <div>filter: drop-shadow(4px 4px 0 #000);</div>
                        </div>
                    `,
                    styles: `
                        .s1-c9 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png);
                            filter: drop-shadow(4px 4px 4px #000);
                        }
                    `
                },
                {
                    title: 'Blur',
                    html: `
                        <div class="s1-c10 wrap twisty-border blue">
                            <h2>Setting filter -> blur:</h2>
                            <div>filter: blur(3px);</div>
                        </div>
                    `,
                    styles: `
                         .s1-c10 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png);
                            filter: blur(3px);
                        }
                    `
                }
            ]
        },
        {
            title: 'Blending Modes',
            concepts: [
                {
                    title: 'Normal',
                    html: `
                        <div class="s2-c1 wrap twisty-border violet">
                            <h2>Setting blend mode -> normal:</h2>
                            <div>background-blend-mode: normal;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c1 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: normal;
                        }
                    `
                },
                {
                    title: 'Multiply',
                    html: `
                        <div class="s2-c2 wrap twisty-border blue">
                            <h2>Setting blend mode -> multiply:</h2>
                            <div>background-blend-mode: multiply;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c2 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: multiply;
                        }
                    `
                },
                {
                    title: 'Screen',
                    html: `
                        <div class="s2-c3 wrap twisty-border violet">
                            <h2>Setting blend mode -> screen:</h2>
                            <div>background-blend-mode: screen;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c3 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: screen;
                        }
                    `
                },
                {
                    title: 'Overlay',
                    html: `
                        <div class="s2-c4 wrap twisty-border blue">
                            <h2>Setting blend mode -> overlay:</h2>
                            <div>background-blend-mode: overlay;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c4 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: overlay;
                        }
                    `
                },
                {
                    title: 'Darken',
                    html: `
                        <div class="s2-c5 wrap twisty-border violet">
                            <h2>Setting blend mode -> darken:</h2>
                            <div>background-blend-mode: darken;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c5 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: darken;
                        }
                    `
                },
                {
                    title: 'Lighten',
                    html: `
                        <div class="s2-c6 wrap twisty-border blue">
                            <h2>Setting blend mode -> lighten:</h2>
                            <div>background-blend-mode: lighten;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c6 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: lighten;
                        }
                    `
                },
                {
                    title: 'Color Dodge',
                    html: `
                        <div class="s2-c7 wrap twisty-border violet">
                            <h2>Setting blend mode -> color-dodge:</h2>
                            <div>background-blend-mode: color-dodge;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c7 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: color-dodge;
                        }
                    `
                },
                {
                    title: 'Color Burn',
                    html: `
                        <div class="s2-c8 wrap twisty-border blue">
                            <h2>Setting blend mode -> color-burn:</h2>
                            <div>background-blend-mode: color-burn;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c8 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: color-burn;
                        }
                    `
                },
                {
                    title: 'Hard Light',
                    html: `
                        <div class="s2-c9 wrap twisty-border violet">
                            <h2>Setting blend mode -> hard-light:</h2>
                            <div>background-blend-mode: hard-light;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c9 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: hard-light;
                        }
                    `
                },
                {
                    title: 'Soft Light',
                    html: `
                        <div class="s2-c10 wrap twisty-border blue">
                            <h2>Setting blend mode -> soft-light:</h2>
                            <div>background-blend-mode: soft-light;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c10 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: soft-light;
                        }
                    `
                },
                {
                    title: 'Difference',
                    html: `
                        <div class="s2-c11 wrap twisty-border violet">
                            <h2>Setting blend mode -> difference:</h2>
                            <div>background-blend-mode: difference;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c11 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: difference;
                        }
                    `
                },
                {
                    title: 'Exclusion',
                    html: `
                        <div class="s2-c12 wrap twisty-border blue">
                            <h2>Setting blend mode -> exclusion:</h2>
                            <div>background-blend-mode: exclusion;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c12 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: exclusion;
                        }
                    `
                },
                {
                    title: 'Hue',
                    html: `
                        <div class="s2-c13 wrap twisty-border violet">
                            <h2>Setting blend mode -> hue:</h2>
                            <div>background-blend-mode: hue;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c13 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: hue;
                        }
                    `
                },
                {
                    title: 'Saturation',
                    html: `
                        <div class="s2-c14 wrap twisty-border blue">
                            <h2>Setting blend mode -> saturation:</h2>
                            <div>background-blend-mode: saturation;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c14 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: saturation;
                        }
                    `
                },
                {
                    title: 'Color',
                    html: `
                        <div class="s2-c15 wrap twisty-border violet">
                            <h2>Setting blend mode -> color:</h2>
                            <div>background-blend-mode: color;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c15 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: color;
                        }
                    `
                },
                {
                    title: 'Luminosity',
                    html: `
                        <div class="s2-c16 wrap twisty-border blue">
                            <h2>Setting blend mode -> luminosity:</h2>
                            <div>background-blend-mode: luminosity;</div>
                        </div>
                    `,
                    styles: `
                        .s2-c16 div:nth-child(2) {
                            width: 350px;
                            height: 45px;
                            padding: 5px 0 0 10px;
                            margin-bottom: 20px;
                            color: white;
                            background: url(../../images/cat_350x50.png) #ff6666;
                            background-blend-mode: luminosity;
                        }
                    `
                }
            ]
        }
    ]
};