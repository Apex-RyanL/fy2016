
/* Chapter 5: Introducing CSS3 2D Transforms */
export const chapter5: IChapter = {
    value: 'Chapter5',  // insert the appropriate selector tag in the main window
    title: 'Chapter 5',
    description: 'Introducing CSS3 2D Transforms',
    commonCSS: `
        div {
            font-family: monospace, sans-serif;
        }
        .twisty-border {
            border-top-left-radius: 7px;
            border-bottom-right-radius: 7px;
        }
        .twisty-border.blue {
            height: 200px;
            background: rgba(2, 137, 209, 0.5);
            color: black;
        }
        .twisty-border.violet {
            height: 200px;
            background: rgba(75, 27, 159,0.5);
            color: white;
        }
        .wrap {
            padding: 20px 5% 0 5%;
        }
    `,
    sections: [
        {
            title: 'The Transform Property',
            concepts: [
                {
                    title: 'Translate',
                    html: `
                        <div class="s1-c1 wrap twisty-border violet">
                            <h2>Setting transform -> translate;</h2>
                            <div>translate(0, 0); (default);</div>
                            <div>translate(100px, 50px);</div>
                        </div>
                    `,
                    styles: `
                        .s1-c1 {
                            padding-bottom: 100px;
                        }
                        .s1-c1 div {
                            position: absolute;
                            padding: 10px 0 0 10px;
                            width: 400px;
                            height: 150px;
                            background-color: rgba(0, 0, 255, 0.4);
                        }
                        .s1-c1 div:last-child {
                            transform: translate(100px, 50px);
                        }
                    `
                },
                {
                    title: 'Skew',
                    html: `
                        <div class="s1-c2 wrap twisty-border blue">
                            <h2>Setting transform -> skew;</h2>
                            <div class="row _1">
                                <div>skewX(10deg);</div>
                                <div>skewX(30deg);</div>
                                <div>skewX(50deg);</div>
                            </div>
                            <div class="row _2">
                                <div>skewY(10deg);</div>
                                <div>skewY(30deg);</div>
                                <div>skewY(50deg);</div>
                            </div>
                            <div class="row _3">
                                <div>skewX(10deg) skewY(10deg);</div>
                                <div>skewX(30deg) skewY(30deg);</div>
                                <div>skewX(50deg) skewY(50deg);</div>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s1-c2 {
                             padding-bottom: 380px;
                        }
                        .s1-c2 .row {
                            margin-bottom: 50px;
                        }
                        .s1-c2 .row div {
                            display: inline-block;
                            margin-left: 50px;
                            width: 150px;
                            height: 100px;
                            color: white;
                            background-color: rgba(255, 0, 255, 0.4);
                        }
                        .s1-c2 .row._1 div:first-child {
                            transform: skewX(10deg);
                        }
                        .s1-c2 .row._1 div:nth-child(2) {
                            transform: skewX(30deg);
                        }
                        .s1-c2 .row._1 div:nth-child(3) {
                            transform: skewX(50deg);
                        }
                        .s1-c2 .row._2 div:first-child {
                            transform: skewY(10deg);
                        }
                        .s1-c2 .row._2 div:nth-child(2) {
                            transform: skewY(30deg);
                        }
                        .s1-c2 .row._2 div:nth-child(3) {
                            transform: skewY(50deg);
                        }
                        .s1-c2 .row._3 div:first-child {
                            transform: skewX(10deg) skewY(10deg);
                        }
                        .s1-c2 .row._3 div:nth-child(2) {
                            transform: skewX(30deg) skewY(30deg);
                        }
                        .s1-c2 .row._3 div:nth-child(3) {
                            transform: skewX(50deg) skewY(50deg);
                        }
                    `
                },
                {
                    title: 'Rotate',
                    html: `
                        <div class="s1-c3 wrap twisty-border violet">
                            <h2>Setting transform -> rotate;</h2>
                            <div>no rotation</div>
                            <div>rotate(-45deg);</div>
                            <div>rotate(90deg);</div>
                        </div>
                    `,
                    styles: `
                        .s1-c3 {
                            padding-bottom: 190px;
                        }
                        .s1-c3 h2 {
                            margin-bottom: 120px;
                        }
                        .s1-c3 div {
                            position: absolute;
                            margin-left: 50px;
                            padding: 5px 0 0 5px;
                            width: 300px;
                            height: 100px;
                            background-color: rgba(0, 0, 255, 0.4);
                        }
                        .s1-c3 div:nth-child(3) {
                            transform: rotate(-45deg);
                        }
                        .s1-c3 div:last-child {
                            transform: rotate(90deg);
                        }
                    `
                },
                {
                    title: 'Scale',
                    html: `
                        <div class="s1-c4 wrap twisty-border blue">
                            <h2>Setting transform -> scale;</h2>
                            <div>scale(2);</div>
                            <div>scale(1); (default)</div>
                            <div>scale(0.5);</div>
                        </div>
                    `,
                    styles: `
                        .s1-c4 {
                            padding-bottom: 300px;
                        }
                        .s1-c4 div {
                            position: absolute;
                            left: 220px;
                            top: 220px;
                            width: 200px;
                            height: 200px;
                            background-color: rgba(255, 0, 255, 0.4);
                            color: white;
                        }
                        .s1-c4 div:nth-child(2) {
                            transform: scale(2);
                        }
                        .s1-c4 div:last-child {
                            transform: scale(0.5);
                        }
                    `
                }
            ]
        },
        {
            title: 'Transform Origin',
            concepts: [
                {
                    title: 'Origin Scale',
                    html: `
                        <div class="s2-c1 wrap twisty-border violet">
                            <h2>Setting transform origin:</h2>
                            <div>origin: 50% 50%; scale(1); (default)</div>
                            <div>origin: 0 0; scale(2);</div>
                        </div>
                    `,
                    styles: `
                        .s2-c1 {
                            padding-bottom: 210px;
                        }
                        .s2-c1 div {
                            background-color: rgba(0, 0, 255, 0.4);
                            margin-left: 50px;
                            margin-bottom: 10px;
                            padding: 5px 0 0 5px;
                            width: 250px;
                            height: 100px;
                        }
                        .s2-c1 div:last-child {
                            transform: scale(2);
                            transform-origin: 0 0;
                        }
                    `
                },
                {
                    title: 'Origin Rotate',
                    html: `
                        <div class="s2-c2 wrap twisty-border blue">
                            <h2>Setting transform origin:</h2>
                            <div>origin: 50% 50%; rotate(-45deg);</div>
                            <div>origin: top left; rotate(45deg);</div>
                        </div>
                    `,
                    styles: `
                        .s2-c2 {
                            padding-bottom: 270px;
                        }
                        .s2-c2 h2 {
                            padding-bottom: 60px;
                        }
                        .s2-c2 div {
                            background-color: rgba(255, 0, 255, 0.4);
                            color: white;
                            margin-left: 100px;
                            padding: 5px 0 0 5px;
                            width: 200px;
                            height: 100px;
                        }
                        .s2-c2 div:nth-child(2) {
                            transform: rotate(-45deg);
                        }
                        .s2-c2 div:last-child {
                            transform: rotate(45deg);
                            transform-origin: top left;
                        }
                    `
                }
            ]
        }
    ]
};