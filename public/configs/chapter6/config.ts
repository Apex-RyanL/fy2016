
/* Chapter 6: Bringing 2D Transforms to Live with Transitions */
export const chapter6: IChapter = {
    value: 'Chapter6',  // insert the appropriate selector tag in the main window
    title: 'Chapter 6',
    description: 'Bringing 2D Transforms to Life with Transitions',
    commonCSS: `
        div {
            font-family: monospace, sans-serif;
        }
        .twisty-border {
            border-top-left-radius: 7px;
            border-bottom-right-radius: 7px;
        }
        .twisty-border.blue {
            height: 200px;
            background: rgba(2, 137, 209, 0.5);
            color: black;
        }
        .twisty-border.violet {
            height: 200px;
            background: rgba(75, 27, 159,0.5);
            color: white;
        }
        .wrap {
            padding: 20px 5% 0 5%;
        }
    `,
    sections: [
        {
            title: 'Introducing CSS Transitions',
            concepts: [
                {
                    title: 'Controlling Your Transitions',
                    html: `
                        <div class="s1-c1 wrap twisty-border violet">
                            <h2>Setting the transition properties WITHOUT shorthand:</h2>
                            <div>transition-property: background-color;</div>
                            <div>transition-duration: 1s;</div>
                            <div>transition-timing-function: linear;</div>
                            <div>transition-delay: .5s;</div>
                            <h2>Setting the transition property WITH shorthand:</h2>
                            <div>transition: [transition-property] [transition-delay] [transition-timing-function] [transition-duration];</div>
                            <div>transition: [transition-timing-function] [transition-delay] [transition-property] [transition-duration];</div>
                            <div>transition: [transition-timing-function] [transition-property] [transition-delay] [transition-duration];</div>
                            <div>transition: [transition-delay] [transition-duration] [transition-property] [transition-timing-function];</div>
                            <div>* All valid shorthand types</div>
                        </div>
                    `,
                    styles: `
                        .s1-c1 {
                            padding-bottom: 100px;
                        }
                        .s1-c1 div {
                            font-size: 11px;
                        }
                        .s1-c1 div:last-child {
                            margin-top: 10px;
                            color: rgba(0, 0, 0, 0.5);
                            font-size: 10px;
                            font-style: italic;
                        }
                    `
                },
                {
                    title: 'Understanding the Transition Property',
                    html: `
                        <div class="s1-c2 wrap twisty-border blue">
                            <div>hover</div>
                        </div>
                    `,
                    styles: `
                        .s1-c2 div {
                            width: 400px;
                            height: 180px;
                            line-height: 180px;
                            text-align: center;
                            font-size: 24px;
                            color: white;
                            background: #66cc66;
                            transition: background 0.5s ease-in-out;
                        }
                        .s1-c2 div:hover {
                            background: #ff6666;
                        }
                    `
                }
            ]
        },
        {
            title: 'Combining Transitions and 2D Transforms',
            concepts: [
                {
                    title: 'Transitioning Rotate',
                    html: `
                        <div class="s2-c1 wrap twisty-border violet">
                            <div></div>
                        </div>
                    `,
                    styles: `
                        .s2-c1 {
                            padding-bottom: 20px;
                        }
                        .s2-c1 div {
                            width: 200px;
                            height: 200px;
                            background: url(http://placekitten.com/g/200/200);
                            transform-origin: 100% 0;
                            transition: 2s cubic-bezier(.55, 2.3, .52, .52);
                        }
                        .s2-c1 div:hover {
                            transform: rotate(-90deg);
                        }
                    `
                },
                {
                    title: 'Transitioning Translate',
                    html: `
                        <div class="s2-c2 wrap twisty-border blue">
                            <div></div> <!-- Ball -->
                            <div></div> <!-- Shadow -->
                        </div>
                    `,
                    styles: `
                        .s2-c2 {
                            padding-bottom: 60px;
                        }
                        .s2-c2 div {
                            width: 200px;
                            height: 200px;
                            border-radius: 50%;
                            background: radial-gradient(40% 40%, #acd9fc 5%, #0090ff 50%, #002f53 90%);
                            transition: 3s;
                        }
                        .s2-c2 div + div {
                            width: 200px;
                            height: 40px;
                            background: radial-gradient(#000 5%, #666 30%, transparent 70%);
                            transition: 3s;
                        }
                        .s2-c2 div:hover {
                            transform: translate(500px) rotate(360deg);
                        }
                        .s2-c2 div:hover + div {
                            transform: translate(500px);
                        }
                    `
                },
                {
                    title: 'Transitioning Scale',
                    html: `
                        <div class="s2-c3 wrap twisty-border violet">
                            <div class="_1">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                            <div class="_2">
                                <div></div>
                                <div></div>
                                <div></div>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s2-c3 {
                            padding-bottom: 40px;
                        }
                        .s2-c3 div {
                            width: 100px;
                            height: 100px;
                            display: inline-block;
                            margin: 0 10px 10px 10px;
                            transition: .4s
                        }
                        .s2-c3 ._1,
                        .s2-c3 ._2 {
                            width: 400px;
                        }
                        .s2-c3 ._1 div:hover {
                            transform: scale(2) rotate(360deg);
                            z-index: 10;
                        }
                        .s2-c3 ._2 div:hover {
                            transform: scale(2) rotate(360deg);
                            z-index: 10;
                        }
                        .s2-c3 ._1 div:first-child {
                            background-color: red;
                        }
                        .s2-c3 ._1 div:nth-child(2) {
                            background-color: green;
                        }
                        .s2-c3 ._1 div:nth-child(3) {
                            background-color: blue;
                        }
                        .s2-c3 ._2 div:nth-child(1) {
                            background-color: yellow;
                        }
                        .s2-c3 ._2 div:nth-child(2) {
                            background-color: purple;
                        }
                        .s2-c3 ._2 div:nth-child(3) {
                            background-color: orange;
                        }
                    `
                },
                {
                    title: 'Transitioning Skew',
                    html: `
                        <div class="s2-c4 wrap twisty-border blue">
                            <div></div>
                        </div>
                    `,
                    styles: `
                        .s2-c4 {
                            padding-bottom: 50px;
                        }
                        .s2-c4 div {
                            margin: 60px 0 0 150px;
                            width: 200px;
                            height: 100px;
                            background-color: rgba(255, 0, 255, 0.4);
                            transform: skewX(-20deg) skewY(-30deg);
                            transition: .5s;
                        }
                        .s2-c4 div:hover {
                            transform: skewX(20deg) skewY(30deg);
                        }
                    `
                }
            ]
        }
    ]
};