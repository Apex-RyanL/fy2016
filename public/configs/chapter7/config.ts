
/* Chapter 7: A New Dimension with 3D Transforms */
export const chapter7: IChapter = {
    value: 'Chapter7',  // insert the appropriate selector tag in the main window
    title: 'Chapter 7',
    description: 'A New Dimension with 3D Transforms',
    commonCSS: `
        div {
            font-family: monospace, sans-serif;
        }
        .twisty-border {
            border-top-left-radius: 7px;
            border-bottom-right-radius: 7px;
        }
        .twisty-border.blue {
            min-height: 200px;
            background: rgba(2, 137, 209, 0.5);
            color: black;
        }
        .twisty-border.violet {
            min-height: 200px;
            background: rgba(75, 27, 159,0.5);
            color: white;
        }
        .wrap {
            padding: 20px 5% 0 5%;
        }
    `,
    sections: [
        {
            title: `It's All About Perspective`,
            concepts: [
                {
                    title: 'The Perspective Property',
                    html: `
                        <div class="s1-c1 wrap twisty-border violet">
                            <h2>Setting the perspective property:</h2>
                            <div class="container1">
                                <div>perspective: 500px;</div>
                            </div>
                            <div class="container2">
                                <div>transform: perspective(500px) rotateX(45deg);</div>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s1-c1 {
                            padding-bottom: 20px;
                        }
                        .s1-c1 .container1 {
                            display: inline-block;
                            margin-left: 40px;
                            perspective: 500px;
                        }
                        .s1-c1 .container1 div {
                            width: 200px;
                            height: 200px;
                            padding: 10px;
                            transform: rotateX(45deg);
                            background: rgba(0, 0, 255, 0.4);
                        }
                        .s1-c1 .container2 {
                            display: inline-block;
                            margin-left: 40px;
                        }
                        .s1-c1 .container2 div {
                            width: 200px;
                            height: 200px;
                            padding: 10px;
                            transform: perspective(500px) rotateX(45deg);
                            background: rgba(0, 0, 255, 0.4);
                        }
                    `
                },
                {
                    title: 'Perspective Origin',
                    html: `
                        <div class="s1-c2 wrap twisty-border blue">
                            <h2>Setting the perspective origin:</h2>
                            <div class="container"><div>perspective-origin: 0, 0; /* Top Left */</div></div>
                            <div class="container"><div>perspective-origin: 50% 0; /* Top Center */</div></div>
                            <div class="container"><div>perspective-origin: 100% 0; /* Top Right */</div></div>
                        </div>
                    `,
                    styles: `
                        .s1-c2 {
                            padding-bottom: 20px;
                        }
                        .s1-c2 .container {
                            width: 200px;
                            height: 200px;
                            margin-bottom: 20px;
                            margin-left: 50px;
                            perspective: 500px;
                        }
                        .s1-c2 .container div {
                            width: 200px;
                            height: 200px;
                            padding: 10px;
                            color: white;
                            background: rgba(255, 0, 255, 0.4);
                            transform: rotateX(45deg);
                        }
                        .s1-c2 .container:nth-child(2) {
                            margin-right: 70px;
                            perspective-origin: 0 0; /* Top Left */
                        }
                        .s1-c2 .container:nth-child(3) {
                            margin-right: 90px;
                            perspective-origin: 50% 0; /* Top Center */
                        }
                        .s1-c2 .container:last-child {
                            perspective-origin: 100% 0; /* Top Right */
                        }
                    `
                },
                {
                    title: 'Maintaining Perspective',
                    html: `
                        <div class="s1-c3 wrap twisty-border violet">
                            <div class="container">
                                <div class="child">
                                    <div class="grandchild"></div>
                                </div>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s1-c3 {
                            padding-bottom: 60px;
                        }
                        .s1-c3 .container {
                            perspective: 300px;
                            position: relative;
                            margin-left: 150px;
                        }
                        .s1-c3 .child {
                            position: absolute;
                            width: 200px;
                            height: 200px;
                            background: black;
                            transform: rotateX(45deg);
                            transform-style: preserve-3d;
                        }
                        .s1-c3 .grandchild {
                            position: absolute;
                            width: 200px;
                            height: 200px;
                            background: #444;
                            transform: rotateX(20deg);
                        }
                    `
                },
                {
                    title: 'Backface Visibility',
                    html: `
                        <div class="s1-c4 wrap twisty-border blue">
                            <h2>Setting the backface visibility:</h2>
                            <div>backface-visibility: visible; (default)</div>
                            <div>backface-visibility: hidden;</div>
                        </div>
                    `,
                    styles: `
                        .s1-c4 {
                            padding-bottom: 20px;
                        }
                        .s1-c4 div {
                            display: inline-block;
                            width: 200px;
                            height: 200px;
                            padding: 10px;
                            margin-right: 30px;
                            font-size: 12px;
                            color: white;
                            background: rgba(255, 0, 255, 0.4);
                            transition: 1s ease-out;
                        }
                        .s1-c4 div:hover {
                            transform: rotateY(180deg);
                        }
                        .s1-c4 div:last-child {
                            backface-visibility: hidden;
                        }
                    `
                }
            ]
        },
        {
            title: 'Moving on to More Transform Properties',
            concepts: [
                {
                    title: 'Rotating in a 3D Environment',
                    html: `
                        <div class="s2-c1 wrap twisty-border violet">
                            <h2>How to rotate in 3D:</h2>
                            <div>rotate3d(1, 0, 0, 180deg); /* Same as rotateX(180deg) */</div>
                            <div>rotate3d(0, 1, 0, 180deg); /* Same as rotateY(180deg) */</div>
                            <div>rotate3d(0, 0, 1, 180deg); /* Same as rotateZ(180deg) */</div>
                            <div class="example">transform: rotate3d(3, 10, 6, 180deg);</div>
                        </div>
                    `,
                    styles: `
                        .s2-c1 {
                            perspective: 1000px;
                            padding-bottom: 140px;
                        }
                        .s2-c1 .example {
                            width: 450px;
                            height: 100px;
                            margin-top: 30px;
                            padding: 10px;
                            background: rgba(0, 0, 255, 0.4);
                            transition: 1s ease-in;
                        }
                        .s2-c1 .example:hover {
                            transform: rotate3d(3, 10, 6, 180deg);
                        }
                    `
                },
                {
                    title: 'Translating in a 3D Environment',
                    html: `
                        <div class="s2-c2 wrap twisty-border blue">
                            <h2>How to translate in 3D:</h2>
                            <div>transform: translateX(10px) translateY(50px) translateZ(20px);</div>
                            <div>/* This is the same as... */</div>
                            <div>transform: translate3d(10px, 50px, 20px);</div>
                            <div class="example">transform: translate3d(100px, 50px, 200px);</div>
                        </div>
                    `,
                    styles: `
                        .s2-c2 {
                            perspective: 1000px;
                            padding-bottom: 100px;
                        }
                        .s2-c2 .example {
                            width: 450px;
                            height: 100px;
                            margin-top: 30px;
                            padding: 10px;
                            background: rgba(255, 0, 255, 0.4);
                            transition: 1s ease-in;
                        }
                        .s2-c2 .example:hover {
                            transform: translate3d(100px, 50px, 200px);
                        }
                    `
                },
                {
                    title: 'Scaling in a 3D Environment',
                    html: `
                        <div class="s2-c3 wrap twisty-border violet">
                            <h2>How to scale in 3D:</h2>
                            <div>transform: scaleX(2) scaleY(1) scaleZ(3);</div>
                            <div>/* This is the same as... */</div>
                            <div>transform: scale3d(2, 1, 3);</div>
                            <div class="example">transform: scale3d(1.5, 2, 2);</div>
                        </div>
                    `,
                    styles: `
                        .s2-c3 {
                            perspective: 1000px;
                            padding-bottom: 140px;
                        }
                        .s2-c3 .example {
                            width: 450px;
                            height: 100px;
                            margin-top: 30px;
                            padding: 10px;
                            background: rgba(0, 0, 255, 0.4);
                            transition: 1s ease-in;
                            transform-origin: 0 0;
                        }
                        .s2-c3 .example:hover {
                            transform: scale3d(1.5, 2, 2);
                        }
                    `
                }
            ]
        },
        {
            title: '3D Cube Experiment',
            concepts: [
                {
                    title: 'Creating a Cube with Transitions',
                    html: `
                        <div class="s3-c1 wrap twisty-border blue">
                            <div class="container">
                                <figure class="cube">
                                    <div class="front">Front</div>
                                    <div class="left">Left</div>
                                    <div class="right">Right</div>
                                    <div class="top">Top</div>
                                    <div class="bottom">Bottom</div>
                                    <div class="back">Back</div>
                                </figure>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s3-c1 {
                            padding-bottom: 100px;
                        }
                        .s3-c1 .container {
                            width: 300px;
                            height: 300px;
                            margin-left: 200px;
                            perspective: 1100px;
                            perspective-origin: -50% -50%;
                        }
                        .s3-c1 .cube {
                            width: 300px;
                            height: 300px;
                            position: relative;
                            transform-style: preserve-3d;
                        }
                        .s3-c1 .cube div {
                            position: absolute;
                            width: 300px;
                            height: 300px;
                            line-height: 300px;
                            text-align: center;
                            color: white;
                            background: rgba(0, 0, 0, 0.8);
                        }
                        /* set faces */
                        .s3-c1 .front   { transform: translateZ(150px); }
                        .s3-c1 .left    { transform: rotateY(-90deg) translateZ(150px); }
                        .s3-c1 .right   { transform: rotateY(90deg) translateZ(150px); }
                        .s3-c1 .top     { transform: rotateX(90deg) translateZ(150px); }
                        .s3-c1 .bottom  { transform: rotateX(-90deg) translateZ(150px); }
                        .s3-c1 .back    { transform: rotateY(180deg) translateZ(150px); }
                    `
                },
                {
                    title: 'Creating a Cube with Transitions',
                    html: `
                        <div class="s3-c2 wrap twisty-border violet">
                                <input id="s3-c2-front" name="transforms" type="radio" value="front" checked>
                                <label for="s3-c2-front">Front</label>
                                <input id="s3-c2-left" name="transforms" type="radio" value="left">
                                <label for="s3-c2-left">Left</label>
                                <input id="s3-c2-right" name="transforms" type="radio" value="right">
                                <label for="s3-c2-right">Right</label>
                                <input id="s3-c2-top" name="transforms" type="radio" value="top">
                                <label for="s3-c2-top">Top</label>
                                <input id="s3-c2-bottom" name="transforms" type="radio" value="bottom">
                                <label for="s3-c2-bottom">Bottom</label>
                                <input id="s3-c2-back" name="transforms" type="radio" value="back">
                                <label for="s3-c2-back">Back</label>
                            <div class="buttons"></div>
                            <div class="container">
                                <figure class="cube">
                                    <div class="front">Front</div>
                                    <div class="left">Left</div>
                                    <div class="right">Right</div>
                                    <div class="top">Top</div>
                                    <div class="bottom">Bottom</div>
                                    <div class="back">Back</div>
                                </figure>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s3-c2 {
                            padding-bottom: 40px;
                        }
                        .s3-c2 .buttons {
                            height: 10px;
                            margin-bottom: 70px;
                        }
                        .s3-c2 .container {
                            width: 300px;
                            height: 300px;
                            margin-left: 200px;
                            perspective: 1100px;
                            perspective-origin: -50% -50%;
                        }
                        .s3-c2 .cube {
                            width: 300px;
                            height: 300px;
                            position: relative;
                            transform-style: preserve-3d;
                            transition: 1s;
                        }
                        .s3-c2 .cube div {
                            position: absolute;
                            width: 300px;
                            height: 300px;
                            line-height: 300px;
                            text-align: center;
                            background: rgba(0, 0, 0, 0.8);
                        }
                        /* set faces */
                        .s3-c2 .front   { transform: translateZ(150px); }
                        .s3-c2 .left    { transform: rotateY(-90deg) translateZ(150px); }
                        .s3-c2 .right   { transform: rotateY(90deg) translateZ(150px); }
                        .s3-c2 .top     { transform: rotateX(90deg) translateZ(150px); }
                        .s3-c2 .bottom  { transform: rotateX(-90deg) translateZ(150px); }
                        .s3-c2 .back    { transform: rotateY(180deg) translateZ(150px) rotateX(-180deg);}
                        /* input buttons event */
                        .s3-c2 input[value="front"]:checked ~ .container .cube {
                            transform: translateZ(-150px) rotateY(0);
                        }
                        .s3-c2 input[value="left"]:checked ~ .container .cube {
                            transform: translateZ(-150px) rotateY(90deg);
                        }
                        .s3-c2 input[value="right"]:checked ~ .container .cube {
                            transform: translateZ(-150px) rotateY(-90deg);
                        }
                        .s3-c2 input[value="top"]:checked ~ .container .cube {
                            transform: translateZ(-150px) rotateX(-90deg);
                        }
                        .s3-c2 input[value="bottom"]:checked ~ .container .cube {
                            transform: translateZ(-150px) rotateX(90deg);
                        }
                        .s3-c2 input[value="back"]:checked ~ .container .cube {
                            transform: translateZ(-150px) rotateX(180deg);
                        }
                    `
                }
            ]
        }
    ]
};