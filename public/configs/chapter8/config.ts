
/* Chapter 8: Getting Animated */
export const chapter8: IChapter = {
    value: 'Chapter8',  // insert the appropriate selector tag in the main window
    title: 'Chapter 8',
    description: 'Getting Animated',
    commonCSS: `
        div {
            font-family: monospace, sans-serif;
        }
        .twisty-border {
            border-top-left-radius: 7px;
            border-bottom-right-radius: 7px;
        }
        .twisty-border.blue {
            min-height: 200px;
            background: rgba(2, 137, 209, 0.5);
            color: black;
        }
        .twisty-border.violet {
            min-height: 200px;
            background: rgba(75, 27, 159,0.5);
            color: white;
        }
        .wrap {
            padding: 20px 5% 0 5%;
        }
    `,
    sections: [
        {
            title: 'Introducing CSS3 Animation',
            concepts: [
                {
                    title: 'Defining Keyframes',
                    html: `
                        <div class="s1-c1 wrap twisty-border violet">
                        <h2>Creating keyframes with identifier: </h2>
                            <pre>
                                <div>@keyframes my-first-animation {</div>
                                <div>    0%  { /* Styles */ }</div>
                                <div>    25% { /* Styles */ }</div>
                                <div>    50% { /* Styles */ }</div>
                                <div>    75% { /* Styles */ }</div>
                                <div>    100% { /* Styles */ }</div>
                                <div>}</div>
                                <div>@keyframes my-first-animation {</div>
                                <div>    from { /* Styles */ }</div>
                                <div>    to   { /* Styles */ }</div>
                                <div>}</div>
                            </pre>
                        </div>
                    `,
                    styles: `
                    `
                },
                {
                    title: 'Applying the Animation',
                    html: `
                        <div class="s1-c2 wrap twisty-border blue">
                            <h2>Setting the animation:</h2>
                            <pre>
                                <div>div {</div>
                                <div>    animation-name: my-first-animation;</div>
                                <div>    animation-duration: 5s;</div>
                                <div>}</div>
                                <div>div {</div>
                                <div>    animation: my-first-animation 5s;</div>
                                <div>}</div>
                            </pre>
                        </div>
                    `,
                    styles: `
                    `
                },
                {
                    title: 'Further Control',
                    html: `
                        <div class="s1-c3 wrap twisty-border violet">
                            <pre>
                                <div>div {</div>
                                <div>    animation-name: my-first-animation;</div>
                                <div>    animation-duration: 10s;</div>
                                <div>    animation-timing-function: ease-in;</div>
                                <div>    /* : ease, linear, ease-in, ease-out, ease-in-out, cubic-bezier(), steps() */</div>
                                <div>    animation-iteration-count: 2;</div>
                                <div>    animation-direction: normal; (default)</div>
                                <div>    /* : reverse, alternate, alternate-reverse */</div>
                                <div>    animation-fill-mode: none; (default)</div>
                                <div>    /* : forwards, backwards, both */</div>
                                <div>}</div>
                                <div>div {</div>
                                <div>    animation: [animation-name] [animation-duration] [animation-timing-function]</div>
                                <div>               [animation-delay] [animation-iteration]</div>
                                <div>               [animation-direction] [animation-fill-mode];</div>
                                <div>}</div>
                            </pre>
                        </div>
                    `,
                    styles: `
                    `
                }
            ]
        },
        {
            title: 'Looking at CSS3 Animations in Action',
            concepts: [
                {
                    title: 'Shifty Circle... Predictable Box',
                    html: `
                        <div class="s2-c1 wrap twisty-border blue">
                            <div class="container">
                                <div class="box"></div>
                                <div class="circle"></div>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s2-c1 {
                            padding-bottom: 20px;
                        }
                        .s2-c1 .container {
                            width: 700px;
                            height: 400px;
                            border: 2px dashed #ccc;
                            position: relative;
                        }
                        .s2-c1 .box {
                            width: 100px;
                            height: 100px;
                            background: rgba(255, 255, 255, 0.5);
                            position: absolute;
                            animation: corners 10s infinite;
                        }
                        .s2-c1 .circle {
                            width: 100px;
                            height: 100px;
                            background: rgba(255, 255, 255, 0.5);
                            position: absolute;
                            border-radius: 50%;
                            animation: corners 5s infinite alternate-reverse cubic-bezier(.4, -0.6, 0.6, 1.6);
                        }
                        @keyframes corners {
                            25% {
                                top: 300px;
                                left: 0;
                                background: rgba(255, 0, 0, 0.4);
                            }
                            50% {
                                top: 300px;
                                left: 600px;
                                background: rgba(0, 255, 0, 0.4);
                            }
                            75% {
                                top: 0;
                                left: 600px;
                                background: rgba(255, 165, 0, 0.4);
                            }
                        }
                    `
                }
            ]
        },
        {
            title: 'Adding Progressive Enhancement with Subtle Animation',
            concepts: [
                {
                    title: 'Fade In Effects',
                    html: `
                        <div class="s3-c1 wrap twisty-border violet">
                            <h2>Applying different animations:</h2>
                            <div>fadeIn</div>
                            <div>fadeInDown</div>
                            <div>lightSpeedIn</div>
                        </div>
                    `,
                    styles: `
                        @keyframes fadeIn {
                            0% {
                                opacity: 0;
                            }
                        }
                        @keyframes fadeInDown {
                            0% {
                                opacity: 0;
                                transform: translateY(-20px);
                            }
                        }
                        @keyframes lightSpeedIn {
                            0% {
                                opacity: 0;
                                transform: translateX(100%) skewX(-30deg);
                            }
                            60% {
                                opacity: 1;
                                transform: translateX(-20%) skewX(30deg);
                            }
                            80% {
                                transform: translateX(0) skewX(-15deg);
                            }
                        }
                        .s3-c1 {
                            padding-bottom: 20px;
                        }
                        .s3-c1 div {
                            display: inline-block;
                            width: 200px;
                            height: 200px;
                            text-align: center;
                            padding: 10px;
                            color: white;
                            background: rgba(0, 0, 255, 0.4);
                        }
                        .s3-c1 div:nth-child(2) {
                            animation: fadeIn 3s ease 5s infinite;
                        }
                        .s3-c1 div:nth-child(3) {
                            animation: fadeInDown 3s ease 5s infinite;
                        }
                        .s3-c1 div:last-child {
                            margin-left: 100px;
                            animation: lightSpeedIn 1.5s ease 5s infinite;
                        }
                    `
                },
                {
                    title: 'Shake',
                    html: `
                        <div class="s3-c2 wrap twisty-border blue">
                            <h2>A Shaky Example:</h2>
                            <div>I Shake!</div>
                        </div>
                    `,
                    styles: `
                        @keyframes shake {
                            10%, 30%, 50%, 70%, 90% {
                                transform: translateX(-10px);
                            }
                            20%, 40%, 60%, 80% {
                                transform: translateX(10px);
                            }
                        }
                        .s3-c2 {
                            padding-bottom: 20px;
                        }
                        .s3-c2 div {
                            width: 200px;
                            height: 200px;
                            padding: 10px;
                            margin-left: 100px;
                            text-align: center;
                            color: white;
                            background: rgba(255, 0, 255, 0.4);
                        }
                        .s3-c2 div:hover {
                            animation: shake 0.75s;
                        }
                    `
                },
                {
                    title: 'Wobble',
                    html: `
                        <div class="s3-c3 wrap twisty-border violet">
                            <div>I Wobble!</div>
                        </div>
                    `,
                    styles: `
                        @keyframes wobble {
                            15% {
                                transform: translateX(-25%) rotate(-5deg);
                            }
                            30% {
                                transform: translateX(20%) rotate(3deg);
                            }
                            45% {
                                transform: translateX(-15%) rotate(-3deg);
                            }
                            60% {
                                transform: translateX(10%) rotate(2deg);
                            }
                            75% {
                                transform: translateX(-5%) rotate(-1deg);
                            }
                        }
                        .s3-c3 {
                            padding-bottom: 20px;
                        }
                        .s3-c3 div {
                            width: 200px;
                            height: 200px;
                            padding: 10px;
                            margin-left: 100px;
                            text-align: center;
                            background: rgba(0, 0, 255, 0.4);
                        }
                        .s3-c3 div:hover {
                            animation: wobble 1s;
                        }
                    `
                }
            ]
        }
    ]
};