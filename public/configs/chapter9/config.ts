
/* Chapter 10: Flexible Box Layout */
export const chapter9: IChapter = {
    value: 'Chapter9',  // insert the appropriate selector tag in the main window
    title: 'Chapter 9',
    description: 'Flexible Box Layout',
    commonCSS: `
        div {
            font-family: monospace, sans-serif;
        }
        .twisty-border {
            border-top-left-radius: 7px;
            border-bottom-right-radius: 7px;
        }
        .twisty-border.blue {
            min-height: 200px;
            background: rgba(2, 137, 209, 0.5);
            color: black;
        }
        .twisty-border.violet {
            min-height: 200px;
            background: rgba(75, 27, 159,0.5);
            color: white;
        }
        .wrap {
            padding: 20px 5% 0 5%;
        }
    `,
    sections: [
        {
            title: 'New Flexbox Properties',
            concepts: [
                {
                    title: 'Establishing the Flex Formatting Context',
                    html: `
                        <div class="s1-c1 wrap twisty-border violet">
                            <h2>Setting display flex:</h2>
                            <section class="container">
                                <div class="bucket">
                                    <h2>Heading One</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Itaque eos id agere, ut a se dolores, morbos, debilitates repellant. Quid est enim aliud esse versutum?</p>
                                </div>
                                <div class="bucket">
                                    <h2>Heading Two</h2>
                                    <p>Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis; Sullae consulatum? Immo alio genere; Confecta res esset.</p>
                                </div>
                                <div class="bucket">
                                    <h2>Heading Three</h2>
                                    <p>Rationis enim perfectio est virtus; Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint. Qua ex cognitione facilior facta est investigatio rerum occultissimarum.</p>
                                </div>
                            </section>
                        </div>
                    `,
                    styles: `
                        .s1-c1 {
                            padding-bottom: 10px;
                        }
                        .s1-c1 .container {
                            display: flex;
                            width: 800px;
                            margin: 50px auto;
                            color: black;
                            background: #eee;
                        }
                        .s1-c1 .bucket {
                            width: 250px;
                            padding: 20px;
                            background: #ccc;
                            border: 1px solid #666;
                        }
                    `
                },
                {
                    title: 'Examining Direction, Flow, and Visual Ordering',
                    html: `
                        <div class="s1-c2 wrap twisty-border blue">
                            <h2>Setting flex flow:</h2>
                            <section class="container">
                                <div class="bucket">
                                    <h2>Heading One</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Itaque eos id agere, ut a se dolores, morbos, debilitates repellant. Quid est enim aliud esse versutum?</p>
                                </div>
                                <div class="bucket">
                                    <h2>Heading Two</h2>
                                    <p>Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis; Sullae consulatum? Immo alio genere; Confecta res esset.</p>
                                </div>
                                <div class="bucket">
                                    <h2>Heading Three</h2>
                                    <p>Rationis enim perfectio est virtus; Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint. Qua ex cognitione facilior facta est investigatio rerum occultissimarum.</p>
                                </div>
                                <div class="bucket">
                                    <h2>Heading Four</h2>
                                    <p>Videamus igitur sententias eorum, tum ad verba redeamus. Itaque hic ipse iam pridem est reiectus; Quia dolori non voluptas contraria est, sed doloris privatio.</p>
                                </div>
                                <div class="bucket">
                                    <h2>Heading Five</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Itaque eos id agere, ut a se dolores, morbos, debilitates repellant. Quid est enim aliud esse versutum?</p>
                                </div>
                                <div class="bucket">
                                    <h2>Heading Six</h2>
                                    <p>Multa sunt dicta ab antiquis de contemnendis ac despiciendis rebus humanis; Sullae consulatum? Immo alio genere; Confecta res esset.</p>
                                </div>
                                <div class="bucket">
                                    <h2>Heading Seven</h2>
                                    <p>Rationis enim perfectio est virtus; Parvi enim primo ortu sic iacent, tamquam omnino sine animo sint. Qua ex cognitione facilior facta est investigatio rerum occultissimarum.</p>
                                </div>
                                <div class="bucket">
                                    <h2>Heading Eight</h2>
                                    <p>Videamus igitur sententias eorum, tum ad verba redeamus. Itaque hic ipse iam pridem est reiectus; Quia dolori non voluptas contraria est, sed doloris privatio.</p>
                                </div>
                            </section>
                        </div>
                    `,
                    styles: `
                        .s1-c2 {
                            padding-bottom: 10px;
                        }
                        .s1-c2 .container {
                            display: flex;
                            width: 800px;
                            margin: 50px auto;
                            color: black;
                            background: #eee;
                            flex-flow: row-reverse wrap; /* shorthand for [flex-direction] and [flex-wrap] */
                        }
                        .s1-c2 .bucket {
                            width: 158px;
                            padding: 20px;
                            background: #ccc;
                            border: 1px solid #666;
                        }
                    `
                },
                {
                    title: 'Controlling Alignment',
                    html: `
                        <div class="s1-c3 wrap twisty-border violet">
                            <h2>How to set align-items:</h2>
                            <h4>stretch (default)</h4>
                            <div class="row">
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit sententias.</div>
                                <div>Lorem ipsum dolor sit amet.</div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing.</div>
                                <div>Lorem ipsum.</div>
                                <div>Lorem ipsum dolor sit amet.</div>
                                <div>Lorem ipsum dolor sit amet, consectetur.</div>
                            </div>
                            <h4>flex-start</h4>
                            <div class="row">
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit sententias.</div>
                                <div>Lorem ipsum dolor sit amet.</div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing.</div>
                                <div>Lorem ipsum.</div>
                                <div>Lorem ipsum dolor sit amet.</div>
                                <div>Lorem ipsum dolor sit amet, consectetur.</div>
                            </div>
                            <h4>flex-end</h4>
                            <div class="row">
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit sententias.</div>
                                <div>Lorem ipsum dolor sit amet.</div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing.</div>
                                <div>Lorem ipsum.</div>
                                <div>Lorem ipsum dolor sit amet.</div>
                                <div>Lorem ipsum dolor sit amet, consectetur.</div>
                            </div>
                            <h4>center</h4>
                            <div class="row">
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit sententias.</div>
                                <div>Lorem ipsum dolor sit amet.</div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing.</div>
                                <div>Lorem ipsum.</div>
                                <div>Lorem ipsum dolor sit amet.</div>
                                <div>Lorem ipsum dolor sit amet, consectetur.</div>
                            </div>
                            <h4>baseline</h4>
                            <div class="row">
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit sententias.</div>
                                <div>Lorem ipsum dolor sit amet.</div>
                                <div>Lorem ipsum dolor sit amet, consectetur adipiscing.</div>
                                <div>Lorem ipsum.</div>
                                <div>Lorem ipsum dolor sit amet.</div>
                                <div>Lorem ipsum dolor sit amet, consectetur.</div>
                            </div>
                        </div>
                    `,
                    styles: `
                        .s1-c3 {
                            padding-bottom: 10px;
                        }
                        .s1-c3 h4 {
                            margin-bottom: 5px;
                        }
                        .s1-c3 .row {
                            display: flex;
                            width: 800px;
                            margin: 20px auto;
                            color: black;
                            background: #eee;
                            justify-content: space-between;
                        }
                        .s1-c3 .row:nth-child(5) {
                            align-items: flex-start;
                        }
                        .s1-c3 .row:nth-child(7) {
                            align-items: flex-end;
                        }
                        .s1-c3 .row:nth-child(9) {
                            align-items: center;
                        }
                        .s1-c3 .row:nth-child(11) {
                            align-items: baseline;
                        }
                        .s1-c3 .row div {
                            width: 138px;
                            text-align: center;
                            border: 1px solid #eee;
                            background: rgba(255, 255, 0, 0.4);
                        }
                    `
                }
            ]
        }
    ]
};