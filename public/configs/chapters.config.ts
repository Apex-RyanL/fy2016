
import { chapter1 } from './chapter1/config';
import { chapter2 } from './chapter2/config';
import { chapter3 } from './chapter3/config';
import { chapter4 } from './chapter4/config';
import { chapter5 } from './chapter5/config';
import { chapter6 } from './chapter6/config';
import { chapter7 } from './chapter7/config';
import { chapter8 } from './chapter8/config';
import { chapter9 } from './chapter9/config';
import { chapter10 } from './chapter10/config';

export const CHAPTERS: any = {

    menu_dropdown: [
        chapter1,
        chapter2,
        chapter3,
        chapter4,
        chapter5,
        chapter6,
        chapter7,
        chapter8,
        chapter9,
        chapter10
    ]
};