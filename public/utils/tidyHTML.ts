
export class Tidy {

    public tidy (_sourceHTML: string): string {
        var splitByLine: Array<string> = _sourceHTML.split('\n');
        var transform: string;
        var numSpaces: number;
        var charArray: Array<string>;

        var i: number, n: number = splitByLine[1].split('').length;
        for (i = 0; i < n; ++i) {
            if (splitByLine[1].charAt(i) === ' ') { numSpaces++; }
            else { break; }
        }

        splitByLine.forEach((line: string) => {
            charArray = line.split('');
            charArray.splice(0, numSpaces);
            line = charArray.join('');
            transform += line + '\n';
        });

        return transform;
    }

}