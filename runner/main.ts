
/// <reference path="../node_modules/angular2/typings/browser.d.ts"/>
/// <reference path="../typings/chapter.d.ts" />

import { PROVIDERS } from '../public/PROVIDERS';

import { AppMain } from './../public/components/app-main/app-main';
import { bootstrap } from 'angular2/platform/browser';

bootstrap(AppMain, [ PROVIDERS ]);