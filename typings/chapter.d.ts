declare interface IChapter {
    value: string;
    title: string;
    description: string;
    sections: Array<ISection>;
    commonHTML?: string;
    commonCSS?: string;
}

declare interface ISection {
    title: string;
    description?: string;
    concepts: Array<IConcept>;
}

declare interface IConcept {
    title: string;
    html: string;
    styles: string;
}